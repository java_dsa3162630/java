/*
Program 1
WAP to take size of array from user and also take integer elements from user Print sum
of odd elements only

Input : Enter size : 5
Enter array elements : 1 2 3 4 5
Output : 9
*/

import java.io.*;
class OddSum{
	
	public static void main(String[] HP)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		System.out.println("Enter Array Size");
		int size = Integer.parseInt(br.readLine());
		int arr[] = new int[size];

		int Oddsum = 0;
		
		System.out.println("Enter Array Elements");
		for(int i=0; i<size ;i++){
			arr[i] = Integer.parseInt(br.readLine());

			if(arr[i]%2 !=0)
				Oddsum=Oddsum+arr[i];
		}
		System.out.println("Odd Elements Sum is "+Oddsum);

	}
}
