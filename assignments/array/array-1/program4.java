/*
Program 4
Write a program, take 7 characters as an input , Print only vowels from the array
Input: a b c o d p e
Output : a o e
*/

import java.io.*;
class Vowels{
	
	public static void main(String[] HP)throws IOException{
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter Array Size");
		int size = Integer.parseInt(br.readLine());
		char arr[] = new char[size];

		System.out.println("Enter Character Array Elenemts");
		for(int j=0; j<size; j++){
			arr[j]=(char)br.read();
			br.skip(1);
		}
		for(int j=0; j<arr.length; j++){
			if(arr[j]== 'a' || arr[j]== 'e' || arr[j]== 'i' || arr[j]== 'o' || arr[j]== 'u' || arr[j]== 'A' || arr[j]== 'E' || arr[j]== 'I' || arr[j]=='O' || arr[j]== 'U')
				System.out.print(arr[j]+" ");
		}
		System.out.println();
	}
}
