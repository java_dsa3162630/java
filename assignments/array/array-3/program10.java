/*
Program 10
Write a program to print the second min element in the array
Input: Enter array elements: 255 2 1554 15 65 95 89
Output: 15
*/

import java.io.*;

class Array{

        public static void main(String[] HP)throws IOException{

                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter array size");
                int size = Integer.parseInt(br.readLine());

                int arr[] = new int[size];

                System.out.println("Enter array elements");
                for(int i=0; i<arr.length; i++)
                        arr[i] = Integer.parseInt(br.readLine());

                for(int i=0; i<arr.length-1; i++){
                        for(int j=0; j<arr.length-i-1; j++){
                                if(arr[j] > arr[j+1]){
                                        int temp = arr[j];
                                        arr[j] = arr[j+1];
                                        arr[j+1] = temp;
                                }
                        }
                }
                System.out.println("Second min element: "+arr[1]);
        }
}
