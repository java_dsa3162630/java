/*
Program 6
WAP to find a palindrome number from an array and return its index.
Take size and elements from the user
Input: 10 25 252 36 564
Output: Palindrome no 252 found at index: 2
*/

import java.io.*;

class Array{

        public static void main(String[] HP)throws IOException{

                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter array size");
                int size = Integer.parseInt(br.readLine());

                int arr[] = new int[size];

                System.out.println("Enter array elements");
                for(int i=0; i<arr.length; i++)
                        arr[i] = Integer.parseInt(br.readLine());

                for(int i=0; i<arr.length; i++){
                        int rem = 0;
			int num = 0;
			int temp = arr[i];

                        while(temp != 0){

				rem = temp%10;
				num = num*10+rem;
				temp = temp/10;
                        }

                        if(num == arr[i])
                                System.out.print("Palindrome number "+arr[i]+" found at index: "+i+" ");
                }
                System.out.println();
        }
}
