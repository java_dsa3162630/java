/*
Program 2
WAP to reverse each element in an array.
Take size and elements from the user
Input: 10 25 252 36 564
Output: 01 52 252 63 465
*/

import java.io.*;

class Array{

        public static void main(String[] HP)throws IOException{

                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter array size");
                int size = Integer.parseInt(br.readLine());

                int arr[] = new int[size];

                System.out.println("Enter array elements");
                for(int i=0; i<arr.length; i++)
                        arr[i] = Integer.parseInt(br.readLine());

                for(int i=0; i<arr.length; i++){
                        int temp = arr[i];
                        int rem = 0;
			int num = 0;

                        while(temp != 0){
                                rem = temp%10;
				num = num*10+rem;
                                temp = temp/10;
                        }

                        arr[i] = num;
                }
                for(int i=0; i<arr.length; i++)
                	System.out.print(arr[i]+" ");
                System.out.println();
        }
}
