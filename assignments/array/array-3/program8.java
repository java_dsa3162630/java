/*
Program 8
WAP to find an ArmStong number from an array and return its index.
Take size and elements from the user
Input: 10 25 252 36 153 55 89
Output: Armstrong no 153 found at index: 4
*/

import java.io.*;

class Array{

	void armStrong(int n, int i){
	
		int count = 0;
		int temp = n;

		while(temp != 0){
			count++;
			temp = temp/10;
		}

		temp = n;

		int sum = 0;
		while(temp != 0){
			sum = sum + pow(temp%10,count);
			temp = temp/10;
		}

		if(n == sum)
			System.out.println("Armstrong number "+n+" found at index: "+i);
	}

	int pow(int n, int c){
	
		int pow = 1;
		for(int i=1; i<=c; i++)
			pow = pow * n;
		return pow;
	}

        public static void main(String[] HP)throws IOException{

                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter array size");
                int size = Integer.parseInt(br.readLine());

                int arr[] = new int[size];

                System.out.println("Enter array elements");
                for(int i=0; i<arr.length; i++)
                        arr[i] = Integer.parseInt(br.readLine());

                Array ar = new Array();
		
		for(int i=0; i<arr.length; i++)
			ar.armStrong(arr[i],i);
        }
}
