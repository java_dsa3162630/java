/*
Program 7
WAP to find a Strong number from an array and return its index.
Take size and elements from the user
Input: 10 25 252 36 564 145
Output: Strong no 145 found at index: 5
*/

import java.io.*;

class Array{

        public static void main(String[] HP)throws IOException{

                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter array size");
                int size = Integer.parseInt(br.readLine());

                int arr[] = new int[size];

                System.out.println("Enter array elements");
                for(int i=0; i<arr.length; i++)
                        arr[i] = Integer.parseInt(br.readLine());

                for(int i=0; i<arr.length; i++){
                        
                        int temp = arr[i];
			int sum = 0;

                        while(temp != 0){
				
				int fact = 1;

                                for(int j=1; j<=temp%10; j++)
					fact = fact*j;

                        	sum = sum + fact;
                                temp = temp/10;
                        }

                        if(sum == arr[i])
                                System.out.print("Strong number "+arr[i]+" found at index: "+i+" ");
                }
                System.out.println();
        }
}
