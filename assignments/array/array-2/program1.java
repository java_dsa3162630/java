/*Q1.WAP to create an array of 'n' integer elements
     where 'n' value should be taken from user
     insert the values from users and print the sum of all the elements in the array
*/

import java.io.*;

class SumOfArr{
	
	public static void main(String[] HP)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter Array Size");
		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];
		
		System.out.println("Enter Array Elements");
		int sum=0;
		for(int i=0; i<arr.length; i++){
			arr[i]=Integer.parseInt(br.readLine());
			sum = sum + arr[i];
		}
		System.out.println("Sum of Array Elements is "+sum);
	}
}
