/*Q3 WAP to find the sum of odd and even numbers in an array
 * i/p=11 12 13 14 15
 * o/p=odd numbers sum=39
 *     even numbers sum=26
*/

import java.io.*;

class SumOfOddEven{

	public static void main(String[] HP)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter array size");
		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];
	
		int Esum=0;
		int Osum=0;

		System.out.println("Enter Array Elements");
		for(int i=0; i<arr.length; i++ ) {
			arr[i]=Integer.parseInt(br.readLine());

			if(arr[i]%2==0)
				Esum=Esum+arr[i];
			else
				Osum=Osum+arr[i];
		}
		System.out.println("sum of Even elements is "+Esum);
		System.out.println("sum of Odd elements is "+Osum);

	}
}
