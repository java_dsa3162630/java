/*Q2 WAP to find the number of even and odd integers ina given array of integers.
  i/p=1 2 5 4 6 7 8
  o/p= No Of Even Elements:4
       No Of Odd Elements:3
*/

import java.io.*;

class OddEvenCount{
	
	public static void main(String[] HP)throws IOException{
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter Array Size");
		int size = Integer.parseInt(br.readLine());


		int arr[] = new int[size];
		int EvenCount=0;
		int OddCount=0;
		System.out.println("Enter Array Elements");
		for(int i=0;i<arr.length; i++){
			
			arr[i]=Integer.parseInt(br.readLine());
			
			if(arr[i]%2==0){
				EvenCount++;
			}else{
				OddCount++;
			}
			
		}
		System.out.println("Even Element Count in array is "+EvenCount);
		System.out.println("Odd Element Count in array is "+OddCount);
	}
}
