/*
Program 9
Write a Java program to merge two given arrays.
Array1 = [10, 20, 30, 40, 50]
Array2 = [9, 18, 27, 36, 45]
Output :
Merged Array = [10, 20, 30, 40, 50, 9, 18, 27, 36, 45]
Hint: you can take 3rd array
*/

import java.io.*;

class Array{

        public static void main(String[] HP) throws IOException {

                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter array size");
                int size = Integer.parseInt(br.readLine());

                int arr1[] = new int[size];
                int arr2[] = new int[size];

                System.out.println("Enter array1 elements");

                for(int i=0; i<arr1.length; i++)
                        arr1[i] = Integer.parseInt(br.readLine());

                System.out.println("Enter array2 elements");

                for(int i=0; i<arr2.length; i++)
                        arr2[i] = Integer.parseInt(br.readLine());

		int arr3[] = new int[2*size];

            	for(int i=0; i<arr3.length; i++){
                        if(i < size)
				arr3[i] = arr1[i];
			else
				arr3[i] = arr2[i-size];
               	}
                
		System.out.print("Merged Array = [");
                for(int i=0; i<2*size-1; i++)
                       	System.out.print(arr3[i]+", ");

               	System.out.print(arr3[2*size-1]+"]");
               	System.out.println();

        }
}
