//3. WAP program to take a String from user and print the largest ASCII character from it.


import java.io.*;

class StringAssignment{

	static char largestASCII(String str){
	
		char strarr[] = str.toCharArray();

		char lascii = strarr[0];

		for(int i=1; i<strarr.length; i++){
		
			if(strarr[i] > lascii)
				lascii = strarr[i];
		}

		return lascii;
	}

	public static void main(String[] HP) throws IOException {
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter String");
		String str = br.readLine();

		System.out.println("Largest ASCII character = "+largestASCII(str));
	}
}
