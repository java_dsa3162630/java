//2. WAP progrom to take String from User And make it in ToggleCase . (for e.g. Core2Web:-> cORE2wEB)


import java.io.*;

class StringAssignment{

	static String toggleString(String str){
	
		char strarr[] = str.toCharArray();

		for(int i=0; i<strarr.length; i++){
		
			if(strarr[i] >= 65 && strarr[i] <= 90)
				strarr[i] = (char) (strarr[i]+32);
			
			else if(strarr[i] >= 97 && strarr[i] <= 122)
				strarr[i] = (char) (strarr[i]-32);
			
			else
				continue;
		}

		String str1 = new String(strarr);

		return str1;
	}

	public static void main(String[] HP) throws IOException {
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter String");
		String str = br.readLine();

		System.out.println("Toggled String = "+toggleString(str));
	}
}
