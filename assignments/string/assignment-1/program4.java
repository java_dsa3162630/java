//4. WAP to To take a String from user and check the String is alphanumeric or not.

import java.io.*;

class StringAssignment{

	static boolean isAlphaNumeric(String str){
	
		char strarr[] = str.toCharArray();

		int aCount = 0;
		int nCount = 0;

		for(int i=0; i<strarr.length; i++){
		
			if((strarr[i] >= 65 && strarr[i] <= 90) || (strarr[i] >= 97 && strarr[i] <= 122))
				aCount++;
			else if(strarr[i] >= 48 && strarr[i] <= 57)
				nCount++;
			else
				continue;
		}

		if(aCount > 0 && nCount > 0)
			return true;
		else
			return false;
	}

	public static void main(String[] HP) throws IOException {
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter String");
		String str = br.readLine();

		if(isAlphaNumeric(str))
			System.out.println("String is Alphanumeric");
		else
			System.out.println("String is not Alphanumeric");
	}
}
