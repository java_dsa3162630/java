//5. WAP to take two strings from user and check the strings are Anagram or not.

import java.io.*;

class StringAssignment{

	static String sort(String str){
	
		char charArr[] = str.toCharArray();

		for(int i=0; i<charArr.length-1; i++){
			for(int j=0; j<charArr.length-1-i; j++){
				if(charArr[j] < charArr[j+1]){
					char temp = charArr[j];
					charArr[j] = charArr[j+1];
					charArr[j+1] = temp;
				}
			}
		}

		String string = new String(charArr);
		return string;
	}

	static boolean isAnagram(String str1, String str2){
	
		String sortStr1 = sort(str1);
		String sortStr2 = sort(str2);

		char strarr1[] = sortStr1.toCharArray();
		char strarr2[] = sortStr2.toCharArray();

		for(int i=0; i<strarr1.length; i++){
		
			if(strarr1[i] != strarr2[i])
				return false;
		}
		return true;
	}

	public static void main(String[] HP) throws IOException {
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter String");
		String str1 = br.readLine();
		
		System.out.println("Enter String");
		String str2 = br.readLine();

		if(str1.length() == str2.length()){
			
			if(isAnagram(str1,str2))
				System.out.println("Strings are Anagram");
			else
				System.out.println("Strings are not Anagram");
		}
		else
			System.out.println("Strings are not Anagram");

	}
}
