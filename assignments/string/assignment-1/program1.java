//1. WAP to take a string from user and print the count of vowel from the string.


//import java.util.*;
import java.io.*;

class StringAssignment{

	static boolean isVowel(char ch){
	
		if(ch == 'a' || ch == 'e' || ch == 'i' || ch == 'o' || ch == 'u' || ch == 'A' || ch == 'E' || ch == 'I' || ch == 'O' || ch == 'U')
			return true;
		else
			return false;
	}

	public static void main(String[] HP) throws IOException {
	
		//Scanner sc = new Scanner(System.in);
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter String");
		//String str1 = sc.nextLine();
		String str1 = br.readLine();

		char str2[] = str1.toCharArray();

		int count = 0;

		for(int i=0; i<str2.length; i++){
			
			if(isVowel(str2[i]))
				count++;
		}

		System.out.println("Vowel count = "+count);
	}
}
