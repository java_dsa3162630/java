/*
Q8. Write a program to print the following pattern

	A	b	C	d
	E	f	G	h
	I	j	K	l
	M	n	O	p

USE THIS FOR LOOP STRICTLY
	
	for(int i =1;i<=4;i++){
		for(j=1;j<=4;j++){}
	}
*/

class Pattern{

        public static void main(String[] HP){

                int N = 4;
                char ch1 = 'A';
                char ch2 = 'a';

                for(int i=1; i<=N; i++){
                        for(int j=1; j<=N; j++){
                                if(j%2 == 0)
                                        System.out.print(ch2+"\t");
                                else
                                        System.out.print(ch1+"\t");
				ch1++;
				ch2++;
                        }
                        System.out.println();
                }
        }
}
