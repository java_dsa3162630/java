/*
Q5. Write a program to print the following pattern

	26	Z	25	Y
	24	X	23	W
	22	V	21	U
	20	T	19	S

USE THIS FOR LOOP STRICTLY

	for(int i =1;i<=4;i++){
		for(j=1;j<=4;j++){}
	}
*/

class Pattern{

        public static void main(String[] HP){

                int N = 4;
		int num = 26;
		char ch = 'Z';

                for(int i=1; i<=N; i++){
                        for(int j=1; j<=N; j++){
                                if(j%2 == 0){
					System.out.print(ch+"\t");
					ch--;
				}else{
					System.out.print(num+"\t");
					num--;
				}
                        }
                        System.out.println();
                }
        }
}
