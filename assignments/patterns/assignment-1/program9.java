/*
Q9. Write a program to print the following pattern
	
	C	B	A
	C	B	A
	C	B	A
*/

class Pattern{

        public static void main(String[] HP){

                for(int i=1; i<=3; i++){
               		char ch = 'C';
                        for(int j=1; j<=3; j++){
                                System.out.print(ch+"\t");
				ch--;
                        }
                        System.out.println();
                }
        }
}
