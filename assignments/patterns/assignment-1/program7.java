/*
Q7. Write a program to print the following pattern
	
	D	D	D	D
	D	D	D	D
	D	D	D	D
	D	D	D	D
*/

class Pattern{

        public static void main(String[] HP){

                for(int i=1; i<=4; i++){
                        for(int j=1; j<=4; j++){
                                System.out.print("D\t");
                        }
                        System.out.println();
                }
        }
}
