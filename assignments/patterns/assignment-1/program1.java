/*
Q1. Write a program to print the following pattern
	
	C2W	C2W	C2W
	C2W	C2W	C2W
	C2W	C2W	C2W
*/

class Pattern{

	public static void main(String[] HP){
	
		for(int i=1; i<=3; i++){
			for(int j=1; j<=3; j++){
				System.out.print("C2W\t");
			}
			System.out.println();
		}
	}
}
