/*
Program 4: Write a java program that checks a number from 0 to 5 and prints its spelling, if the number is greater than 5 print the number is greater than 5

Input1 : var= 4
Output: four
Input2 : var = 6
Output: number is greater than 5
Input3 : var = -6
Output: ???
*/

class Statements{

	public static void main(String[] args){

		int num = -5;

		if(num == 0 )
			System.out.println("Zero");
		else if(num == 1)
			System.out.println("One");
		else if(num == 2)
			System.out.println("Two");
		else if(num == 3)
			System.out.println("Three");
		else if(num == 4)
			System.out.println("Four");
		else if(num == 5)
			System.out.println("Five");
		else if(num > 5)
			System.out.println("Number is Greater than 5");
		else
			System.out.println("Number is less than 0");
	}
}

			
