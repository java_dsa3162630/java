/*
Program 1: Write a java program to check if a number is even or odd.

Input: var=10;
Output: 10 is an even no
Input: var=37;
Output: 37 is an odd no
Input : var = 0
Output: ???
*/

class Statements{

	public static void main(String[] args){

		int num = 2;

		if(num % 2 == 0)
			System.out.println(num+ " is even number");
		else
			System.out.println(num+ " is odd number");
	}
}
