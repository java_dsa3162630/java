/*
Program 7: Calculate profit or loss. Write a program that takes the cost price and selling price (take it hardcoded) and calculates its profit or loss

input1:
sellingPrice = 1200
costPrice = 1000
Output: profit of 200

input2:
sellingPrice = 300
costPrice = 500
Output: loss of 200

input3:
sellingPrice = 900
costPrice = 900
Output: ???
*/
class Statements{

	public static void main(String[] args){

		float sPrice = 500f;
		float cPrice = 500f;

		if(sPrice > cPrice)
			System.out.println("Profit of"+(sPrice-cPrice));
		else if(sPrice < cPrice)
			System.out.println("Loss of"+(cPrice-sPrice));
		else
			System.out.println("No Profit No Loss");
	}
}
