/*
Program 6: write a program to find a maximum between three numbers

inputs1 :
num1 = 1
num2 = 2
num3 = 3
Output: 3 is the maximum between 1, 2 and 3

inputs2 :
num1 = 1
num2 = 4
num3 = 3
Output: 4 is the maximum between 1, 4 and 3

inputs3 :
num1 = 42
num2 = 32
num3 = 42
Output: ?????
*/

class Statements{

	public static void main(String[] args){

		int a = 4, b = 4, c = 5;

		if(a>b && a>c)
			System.out.println(a+" is greater among "+a+","+b+ " and " +c);
		else if(b>a && b>c)
			System.out.println(b+" is greater among "+a+","+b+ " and " +c);
		else if(c>a && c>b)
			System.out.println(c+" is greater among "+a+","+b+ " and " +c);
		else if(a==b && a>c)
			System.out.println(a+" and "+b+" are equal and maximum among "+a+", "+b+" and "+c);
		else if(a==c && a>b)
			System.out.println(a+" and "+c+" are equal and maximum among "+a+", "+b+" and "+c);
		else if(b==c && b>a)
			System.out.println(b+" and "+c+" are equal and maximum among "+a+", "+b+" and "+c);
		else
			System.out.println(a+","+b+" and "+c+" are all equal");
	}
}

















