//WAP to print the sum of all even numbers and multiplication of odd numbers between 1 to 10

class Statements{

	public static void main(String[] args){

		int i = 1;
		int sum = 0;
		int mul = 1;

		while(i <= 10){
			if(i%2==0)
				sum=sum+i;
			else 
				mul=mul*i;
			i++;
		}
	System.out.println("Sum of even number is "+sum);
	System.out.println("Multiplication of odd numbers is "+mul);
	}
}

