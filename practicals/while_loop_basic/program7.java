/*
Take N as integer input. If N is even print numbers from N to 1 and if N is odd print N to 1 by the difference of 2.
*/

class Statements{

        public static void main(String[] args){

                int N = 10;

		if(N%2 == 0){
			while(N >= 1){
                       		System.out.println(N);
                        	N--;
                	}
		}else{
			while(N >= 1){
                       		System.out.println(N);
                        	N = N-2;
                	}
		}
	}
}


