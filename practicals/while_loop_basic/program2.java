//FACTORIAL OF GIVEN NUMBER
class Statement{

	public static void main(String[] args){

		int n = 6;
		int fact = 1;

		while(n>1){
			fact = fact*n;
			n--;
		}
		System.out.println("Factorial ="+fact);
	}
}
