//Reverse the given number.

class Statements{

        public static void main(String[] args){

                int n = 12345;
		int temp = n;
                int rem;
		int rev = 0;

		if(n == 0){
			System.out.println("Original = "+n);
			System.out.println("Reverse = "+n);
		}else{
			while(n != 0){
                       		rem = n%10;
				rev = rev*10 + rem;
                        	n = n/10;
                	}
                	System.out.println("Original = "+temp);
                	System.out.println("Reverse = "+rev);
		}
        }
}


