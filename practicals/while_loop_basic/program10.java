//Check the given number is Palindrome or not.

class Statements{

        public static void main(String[] args){

                int n = -121;
                int temp = n;
                int rem;
                int rev = 0;

		if(n == 0)
			System.out.println("0 is Palindrome");
		else{
			while(n != 0){
                        	rem = n%10;
                        	rev = rev*10 + rem;
                        	n = n/10;
                	}
			if(temp == rev)
				System.out.println(temp+" is Palindrome");
			else
	        	        System.out.println(temp+" is Not Palindrome");
        	}
	}
}

