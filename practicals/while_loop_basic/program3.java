//count the digits of given number

class Statements{

	public static void main(String[] args){

		int n = 123456789;
		int count = 0;

		while(n !=0){
			count++;
			n=n/10;
		}
		System.out.println("count= "+count);
	}
}
