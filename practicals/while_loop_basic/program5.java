//print square of even digit of given number

class Statements{

	public static void main(String[] args){

		int n = 942111423;
		int temp;
		while(n!=0){
			temp=n%10;
			if(temp%2==0)
				System.out.println(temp*temp);
			n=n/10;
		}
		
	}
}
