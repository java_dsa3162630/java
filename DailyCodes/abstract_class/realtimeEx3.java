abstract class SPPU{

	void Syllabus(){
	
		System.out.println("SPPU Syllabus Common for all pune collages");
	}
	abstract void ClgFee();
}
class Sinhagad extends SPPU{

	void ClgFee(){
		System.out.println("sinhagad fees-55,000");
	}
}
class Zeal extends SPPU{
	
	void ClgFee(){
		System.out.println("zeal fees-50,000");
	}
}
class Client{
	public static void main(String[] HP){
	
		SPPU obj = new Sinhagad();
		obj.Syllabus();
		obj.ClgFee();
		
		SPPU obj1 = new Zeal();
		obj1.Syllabus();
		obj1.ClgFee();
	}
}
