abstract class Parent{

	void carrier(){
		System.out.println("Engineer");
	}
	abstract void marry();
}
class Child extends Parent{

	void marry(){
		System.out.println("Alia bhatt");
	}
}
class Client{
	public static void main(String[] HP){
		Parent obj = new Child();
	        obj.carrier();
		obj.marry();	
	}
}
