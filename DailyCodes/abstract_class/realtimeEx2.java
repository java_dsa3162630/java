abstract class Defence{

	void aim(){
		System.out.println("Aim to protect nation");
	}
	abstract void weapons();
}
class Navy extends Defence{

	void weapons(){
		System.out.println("Sub-marine,armoured ships");
	}
}
class AirForce extends Defence{

	void weapons(){
		System.out.println("Tejas fighter Jet");
	}
}
class BSF extends Defence{

	void weapons(){
		System.out.println("AK47,Tanker");
	}
}
class Client{

	public static void main(String[] HP){
	
		Defence obj = new Navy();
		obj.aim();
		obj.weapons();
		
		Defence obj1 = new AirForce();
		obj1.aim();
		obj1.weapons();
		
		Defence obj2 = new BSF();
		obj2.aim();
		obj2.weapons();
	}
}
