abstract class RBI{

	void RulesRegulations(){
		System.out.println("Rules and Regulations common for all banks");
	}
	abstract void RateOfIntrest();
}
class SBI extends RBI{

	void RateOfIntrest(){
		System.out.println("SBI ROI is 7%");
	}
}
class HDFC extends RBI{
	
	void RateOfIntrest(){
		System.out.println("HDFC ROI is 8%");
	}
}
class Client{

	public static void main(String[] HP){
	
		RBI obj = new SBI();
		obj.RulesRegulations();
		obj.RateOfIntrest();

		RBI obj1 = new HDFC();
		obj1.RulesRegulations();
		obj1.RateOfIntrest();

	}
}
