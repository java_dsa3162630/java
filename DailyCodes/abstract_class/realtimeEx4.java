abstract class Franchaise{
	
	void Test(){
		System.out.println("Quality same as main Branch");
	}
	abstract void price();
}
class PuneFranchaise extends Franchaise{
	
	void price(){
		System.out.println("Pune Franchaise price 100");
	}
}
class SolapurFranchaise extends Franchaise{

	void price(){
		System.out.println("Solapur Frachaise price 120");
	}
}
class Client{
	public static void main(String[] HP){
	
		Franchaise obj = new PuneFranchaise();
		obj.Test();
		obj.price();
		
		Franchaise obj1 = new SolapurFranchaise();
		obj1.Test();
		obj1.price();
	}
}
