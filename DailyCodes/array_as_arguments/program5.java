class ArrayDemo{

	static void fun(int array[]){
	
		System.out.println("IN FUN");
		for(int HP :array){
			System.out.println(HP);
		}
	}
	
	public static void main(String[] HP){
	
		System.out.println("IN MAIN");
		int arr[] = {10,20,30,40};

		for(int x : arr){
		System.out.println(x);
		}

		fun(arr);
	}
}
