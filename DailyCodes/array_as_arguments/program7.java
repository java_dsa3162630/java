class ArrayDemo{

	static void fun(int arr[]){
	
		for(int x :arr)
			System.out.println(x);

		for(int i=0; i<arr.length; i++){
		
			arr[i]=arr[i]+50;//change will be common for both methods
		}
	}
	
	public static void main(String[] HP){
		
		int arr[] = {50,100,150};

		fun(arr);//internally as fun(arrays refrence like pointer)

		for(int x :arr)
			System.out.println(x);
		
	}
}
