class ArrarDemo{

	static void fun(int arr[]){
		for(int x : arr)
			System.out.println(x);

		arr[0] = 100;//change will be common for all
	
	}
	
	public static void main(String[] HP){

		int arr[] = {10,20,30}; //arrays object is in heap section

		for(int x : arr)
			System.out.println(x);

		fun(arr);
		
		for(int x : arr)
			System.out.println(x);
	
	}
}
