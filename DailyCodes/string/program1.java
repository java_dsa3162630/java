class StringDemo{

	public static void main(String[] HP){
	
		String str1 = "core2web";   	//scp
		String str2 = new String("core2web");	//heap new obj
		char str3[] = {'c','2','w'}; 	//heap new obj

		System.out.println(str1);
		System.out.println(str2);
		System.out.println(str3);
	}
}
