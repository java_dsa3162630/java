class StringDemo{

	public static void main(String[] HP){
	
		String str1 = "Pritam";
		String str2 = new String("Pritam");

		if(str1 == str2)	// == operator internally checks iHC
			System.out.println("Equal");
		else
			System.out.println("Not Equal");

		if(str1.equals(str2))	// equals method checks content
			System.out.println("Equal");
		else
			System.out.println("Not Equal");
			
	}
}
