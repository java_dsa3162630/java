import java.io.*;

class MyStrLen{

	static int mystrlen(String str){
		char arr[]=str.toCharArray();

		int count = 0;
		for(int i = 0; i<arr.length; i++)
			count++;

		return count;
	}

	public static void main(String[] HP)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter First String ");
		String str1 = br.readLine();
		
		System.out.println("Enter Second String");
		String str2 = br.readLine();

		if(mystrlen(str1)==mystrlen(str2))
			System.out.println("both string's length is equal");
		else
			System.out.println("both string's length is NOT equal");
	}
	
}
