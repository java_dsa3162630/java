class StringDemo{

	public static void main(String[] HP){
	
		String str1 = "Pritam";
		String str2 = "Hiras";

		System.out.println(str1+str2);

		String str3 = "PritamHiras";

		String str4 = str1+str2;//operation performed on runtime hence new object is created

		System.out.println(System.identityHashCode(str3));
		System.out.println(System.identityHashCode(str4));
	}
}
