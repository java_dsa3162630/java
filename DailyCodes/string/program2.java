class StringDemo{
	
	public static void main(String[] HP){
		
		String str1 = "core2web"; 
		String str2 = new String("core2web");

		System.out.println(System.identityHashCode(str1));//scp str1 & str3 same IHC
		System.out.println(System.identityHashCode(str2));// heap new obj
		
		String str3 = "core2web";
		String str4 = new String("core2web");
		
		System.out.println(System.identityHashCode(str3));//scp str1 & str3 same IHC
		System.out.println(System.identityHashCode(str4));// heap new obj
	}
}
