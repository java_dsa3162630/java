//Hash code checks the content only

class HashCodeDemo{

	public static void main(String[] HP){

		String str1 = "Pritam";
		String str2 = new String("Pritam");
		
		String str3 = "Pritam";
		String str4 = new String("Pritam");

		System.out.println(str1.hashCode());
		System.out.println(str2.hashCode());
		System.out.println(str3.hashCode());
		System.out.println(str4.hashCode());
	}
}
