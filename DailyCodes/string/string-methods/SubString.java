//Method: public string subString(int indes)
//Description: creates a substring of the given string starting at a specified index and ending at the end of the given string
//parameter: ingeter(index of the string)
//return type: String

class SubStringDemo{

	public static void main(String[] HP){

		String str = "Core2Web Tech";

		System.out.println(str.substring(5));
		System.out.println(str.substring(0,4));
	}
}
