//Method : public String concat(String str);
//concat String to this String ie,another string is concatinated with first string

class ConcatDemo{

	public static void main(String[] HP){
	
		String str1 = "Pritam";
		String str2 = "Hiras";
		System.out.println("String 1 = "+str1);
		System.out.println("String 2 = "+str2);
		
		String str3 = str1.concat(str2);
		System.out.println("concatinated String ="+str3);
	}
}
