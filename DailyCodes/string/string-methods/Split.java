//Method:public String[] split(split delimiter)
//Description: split the string around matches of regular expression
//parameter: delimiter(pattern to match)
//return type: String[](array of split strings)

class SplitDemo{

	public static void main(String[] HP){
	
		String str = "Know the code till the core";
		
		String[] strResult = str.split(" ");
		for(String a: strResult)
			System.out.println(a);
	}
}
