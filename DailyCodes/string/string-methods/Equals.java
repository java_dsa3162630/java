//Method: public boolean equals(Object anObject)
//Description: predicate which compares and object to this .this is true only for strings with the same character sequence
//returns true if anObject is semantically equal to this.
//parameter: object(anObject).
//return type: boolean

class EqualsDemo{

	public static void main(String[] HP){

		String str1 = "Pritam";
		String str2 = new String("Pritam");

		System.out.println(str1.equals(str2));//compares the content only and == iHC
	}
}
