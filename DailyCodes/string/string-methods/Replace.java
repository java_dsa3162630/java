//Method:public String replace(char oldChar, char newChar)
//description:replaces every instance of a character in the given String with a new character
//parameter:character(old character),character(new character)
//return type: String

class ReplaceDemo{

	public static void main(String[] HP){
		String str = "Hello World!";

		String result = str.replace('e','a');
		System.out.println(result);
	}
}
