//Method:public int compareToIgnoreCase(String str)
//Description: it compares str1 and str2 (case insensitive)
//parameter: String
//return type: int

class CompareToIgnoreCaseDemo{

	public static void main(String[] HP){

		String str1 = "PRITAM";
		String str2 = "pritam";

		System.out.println(str1.compareToIgnoreCase(str2));	
	}
}
