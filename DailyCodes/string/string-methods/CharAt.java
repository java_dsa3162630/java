//Method: public char charAt(int index);
//description: it returns the character located at specified index within the given string
//parameter:integer(index)
//return type: character

class CharAtDemo{

	public static void main(String[] HP){

		String str = "PritamHiras";

		System.out.println(str.charAt(0));
		System.out.println(str.charAt(1));
		System.out.println(str.charAt(10));
	}
}
