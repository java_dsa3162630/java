//Method:public boolean endsWith(String suffix);
//Description: predicate which determines if the string ends with given suffix
//if suffix is an empty string, true is returned
//throws nullptrexception if string is null
//parameter:prefix string to compare
//return type: boolean

class EndsWithDemo{

	public static void main(String[] HP){
	
		String str1 = "Know the code till the core";

		System.out.println(str1.endsWith("core"));
	}
}
