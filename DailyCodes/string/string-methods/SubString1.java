//Method:public String substring(int i, int j)
//Description: creates a substring of the given string at a specified index and ending at one character before the specified index
//parameter:integer(start index),integer(end index)
//return type : String

class SubSringDemo{

	public static void main(String[] HP){
		String str = "Core2Web";

		System.out.println(str.substring(5,8));
	}
}
