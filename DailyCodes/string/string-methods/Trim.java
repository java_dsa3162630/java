//Method: public String trim();
//Description: trims all the white spaces before and after of the string
//parameter: no parameter
//return type: String

class TrimDemo{

	public static void main(String[] HP){

		String str = "       Know the code till the core";

		System.out.println(str.trim());
	}
}
