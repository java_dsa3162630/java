//Method:public boolean startsWith(String prefix,int toffset)
//Description: predicate which determines if the given string contains the given prefix
//beginning comparison at toffset
//the result is false if the toffset is negative or greater than str.length()
//parameter: prefix string to compare,offset offset for this string where the compariosn starts.
//return type: boolean

class StartsWithDemo{

	public static void main(String[] HP){
	
		String str = "Core2Web";

		System.out.println(str.startsWith("or",1));
	}
}
