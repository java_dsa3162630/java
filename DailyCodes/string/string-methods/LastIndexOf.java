//Method:public int lastIndexOf(int ch,int fromIndex)
//Description:finds the last instance of the character in given string
//parameter: character(ch to find),integer(index to start to search)
//return type : int

class LastIndexDemo{

	public static void main(String[] HP){
	
		String str = "Shashi";

		System.out.println(str.lastIndexOf('h',5));
	}
}
