//Method: public boolean equalsIgnoreCase(string anothetstring)
//description: compares a string to this ignoring cases
//parameter: string(str2)
//return type:boolean

class EqualsIgnoreCaseDemo{

	public static void main(String[] HP){
	
		String str1 = "Core2Web";
		String str2 = "core2web";

		System.out.println(str1.equalsIgnoreCase(str2));
	}

}
