//Method : public int length();
//description: it returns the number of characters contained in given String
//parameter:No parameter
//return type: int

class LengthDemo{

	public static void main(String[] HP){
	
		String str = "PritamHiras";

		System.out.println("given String is "+str);
		System.out.println("length of string is "+str.length());
	}
}
