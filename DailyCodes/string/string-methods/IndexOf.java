//Method: public int indexOf(int ch,int from index)
//Description:finds the first instance of character in given string
//parameter:character(ch to find,ingeter(index to start to search))
//return type:int

class IndexOfDemo{

	public static void main(String[] HP){
		String str1 = "Shashi";

		System.out.println(str1.indexOf('h',0));//1
		System.out.println(str1.indexOf('h',1));//1
		System.out.println(str1.indexOf('h',2));//4
	}
}
