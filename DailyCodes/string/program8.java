class StringDemo{
	
	public static void main(String[] HP){
	
		String str1 = "Pritam";
		String str2 = "Hiras";

		System.out.println(System.identityHashCode(str1));
		System.out.println(System.identityHashCode(str2));

		String str3 = str1+str2;	//calls string builders append method and its return type is new string
		String str4 = str1.concat(str2);

		System.out.println(str3);
		System.out.println(str4);
	
		System.out.println(System.identityHashCode(str3));//diff iHc
		System.out.println(System.identityHashCode(str4));//diff iHC
		
	}
}
