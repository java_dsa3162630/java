class MyStrLength{

	static int MyLengthFun(String str){	//userdefined
		
		char arr[] = str.toCharArray();	//toCharArray() converts string to character array
		
		int count = 0;
		for(int i=0; i<arr.length; i++){
			count++;
		}
		return count;
	}

	public static void main(String[] HP ){
	
		String str = "PritamHiras";

		System.out.println(str.length()); 	//predefined

		int len = MyLengthFun(str);

		System.out.println(len);


	}
}
