class Employee{

	int EmpId = 10;// seperate for each object
	String EmpName = "Kanha";//seperate for each object

	static int y = 20;//common for all

	void EmpInfo(){
		System.out.println("ID="+EmpId);
		System.out.println("Name="+EmpName);
		System.out.println("y="+y);
	}
}
class Main{

	public static void main(String[] HP){
	
		Employee emp1 = new Employee();
		Employee emp2 = new Employee();

		emp1.EmpInfo();
		emp2.EmpInfo();

		System.out.println("-----------------");

		emp2.EmpId = 100;
		emp2.EmpName = "Rahul";
		emp2.y = 5000;//satic y hence change commmon for all
		
		emp1.EmpInfo();
		emp2.EmpInfo();

	}
}
