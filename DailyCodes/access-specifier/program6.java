class ClassRoom{

	String str1 = "Notebook";
        static String str2 = "blackboard";

	void Disp(){
		System.out.println(str1);
		System.out.println(str2);
	}	
}
class Main{

	public static void main(String[] HP){
	
		ClassRoom obj1 = new ClassRoom();
		ClassRoom obj2 = new ClassRoom();

		obj1.Disp();
		obj2.Disp();

		System.out.println("-------------");

		obj2.str1 = "PersonalNotes";
		obj2.str2 = "GoogleClassroom";
		
		obj1.Disp();
		obj2.Disp();


	}
}
