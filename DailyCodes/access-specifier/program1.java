//ACCES SPECIFIER: Default 
//it can be accessed in same class, Different class, Different file, ("BUT CANNOT ACCESS FROM ANOTHER FOLDER")
class Core2Web{

	int NoOfCourses = 5;
	String FavCourse = "Java";

	void fun(){
		System.out.println(NoOfCourses);
		System.out.println(FavCourse);
	}
}

class Student{
 
	public static void main(String[] HP){
	
		Core2Web obj = new Core2Web();
		obj.fun();

		System.out.println(obj.NoOfCourses);
		System.out.println(obj.FavCourse);
	}
}

