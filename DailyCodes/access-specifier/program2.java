//ACCES SPECIFIER: private
//it can be accessed in same class only,("it cannot accessed in other class,file,folder")
class Core2Web{

        int NoOfCourses = 5;
        private String FavCourse = "Java";

	void fun(){
	
		System.out.println(NoOfCourses);
		System.out.println(FavCourse);
	}
}

class Student{

        public static void main(String[] HP){

                Core2Web obj = new Core2Web();
		obj.fun();

                System.out.println(obj.NoOfCourses);
                System.out.println(obj.FavCourse);//error:FavCourse has private access in class Core2Web
        }
}
