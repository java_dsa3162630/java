class Demo{

	int x =10;
	private int y = 20;
	static int z = 30;

	void disp(){
	
		System.out.println(x);
		System.out.println(y);
		System.out.println(z);
	}
}
class Main{
	public static void main(String[] HP){
		
		Demo obj1 = new Demo();
		Demo obj2 = new Demo();

		obj1.disp();

		System.out.println("-------------");
		
		obj2.x = 1000;
		obj2.z = 3000;
		
		obj1.disp();
		obj2.disp();
	}
}
