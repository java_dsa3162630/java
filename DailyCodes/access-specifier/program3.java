class Demo{
	
	int x = 10;
	private int y = 20;

	void fun(){
		System.out.println(x);//10
		System.out.println(y);//20
	}
}
class MainDemo{
	public static void main(String[] HP){
	
		Demo obj = new Demo();

		obj.fun();
		System.out.println(obj.x);//10
		System.out.println(obj.y);//error y is private
		
		System.out.println(x);//error 
		System.out.println(y);//error 
	}
}
