class StringBufferDemo{

	public static void main(String[] HP){
	
		StringBuffer sb = new StringBuffer();

		System.out.println(sb);	//blank
		System.out.println(sb.capacity());//16
		
		sb.append("Shashi");
		System.out.println(sb);//shashi
		System.out.println(sb.capacity());//16
		
		sb.append("Bagal");
		System.out.println(sb);//shashibagal
		System.out.println(sb.capacity());//16
		
		sb.append("Core2Web");
		System.out.println(sb);//shashibagalcore2web
		System.out.println(sb.capacity());//formulae (current capacity + 1)*2
	}
}
