class SBDemo{

	public static void main(String[] HP){
	
		StringBuffer sb = new StringBuffer(100);	//initial capacity we can provide
		
		sb.append("Core2Web");
		sb.append("Biencaps");

		System.out.println(sb); 	//Core2WebBiencaps
		System.out.println(sb.capacity());	//100

		sb.append("Incubator");
		System.out.println(sb); 	//Core2WebBiencapsIncubator
		System.out.println(sb.capacity());	//100

	}
}
