//VVVIMP Code 
//String reverse
class C2W{

	public static void main(String[] HP){
	
		String str1 = "PritamHiras";

		StringBuffer sb = new StringBuffer(str1);

		str1 = sb.reverse().toString();
		//reverse method returns StringBuffer that we cant store in str1 that is String class
		//toString() is a method that converts StringBuffer class to String class

		System.out.println(str1);

		
	}
}
