class SBDemo{

	public static void main(String[] HP){
	
		String str1 = "Shashi";
		String str2 = new String("Bagal");
		StringBuffer str3 = new StringBuffer("Core2Web");

		//String str4 = str1.append(str3); 	//error:cannot find symbol
							//append method is of StringBuffer class
		
		//String str4 = str3.append(str1);	//erroe:incompatible types SBuff cant be conv to String
		
		StringBuffer str4 = str3.append(str1);// append returns new StingBuffer(core2webshashi);

		System.out.println(str1);
		System.out.println(str2);
		System.out.println(str3);
		System.out.println(str4);
	}
}
