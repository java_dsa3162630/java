class StringBufferDemo{

	public static void main(String[] HP){
	
		StringBuffer str1 = new StringBuffer("");

		System.out.println(str1.capacity()); // StringBuffer by default capacity is 16

		StringBuffer str2 = new StringBuffer("pritam");
	
		System.out.println(str2.capacity());//6(pritam)+16(bydefault)
	}
}
