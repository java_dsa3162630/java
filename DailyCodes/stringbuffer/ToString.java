/*METHOD: toString();
 * Description: converts StringBuffer to String
 * parameter:NO parameter
 */

class ToStringDemo{

	public static void main(String[] HP){
	
		StringBuffer str1 = new StringBuffer("Know the code till the core");
		String str2 = "Core2Web: ";

		String str3 = str1.toString();

		String str4 = str2.concat(str3);

		System.out.println(str4);
	}
}
