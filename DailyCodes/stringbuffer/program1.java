class StringBufferDemo{

	public static void main(String[] HP){

		String str1 = "Pritam";
		str1 = str1.concat("Hiras");	//'concat'method will create a new object and write 'PritamHiras' in it	
		System.out.println(str1);	//PritamHiras

		StringBuffer str2 = new StringBuffer("Pritam");
		str2.append("Hiras");		//'append'method will create 'PritamHiras' to the same previous object of str2
		System.out.println(str2);	//PritamHiras
	}
}
