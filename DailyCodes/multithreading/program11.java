//2.join() (throws InterruptedException)

class MyThread extends Thread{

	public void run(){
	
		for(int i =0; i<10; i++){
		
			System.out.println("In Thread-0");
		}
	}
}
class ThreadDemo{

	public static void main(String[] HP)throws InterruptedException{
	
		MyThread obj = new MyThread();
		obj.start();

		obj.join();//gives priority to Thread-0 (main thread will start execution after complition of Thread-0)

		for(int i=0; i<10; i++){
		
			System.out.println("In Main");
		}
	}
}
