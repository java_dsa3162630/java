//Child ThreadGroup
class MyThread extends Thread{

	MyThread(ThreadGroup tg,String str){
	
		super(tg,str);
	}
	public void run(){
	
		System.out.println(Thread.currentThread());
	}
}
class ThreadDemo{

	public static void main(String[] HP){
	
		ThreadGroup pThreadgp = new ThreadGroup("C2W");
		MyThread obj1 = new MyThread(pThreadgp,"C");
		MyThread obj2 = new MyThread(pThreadgp,"Java");
		MyThread obj3 = new MyThread(pThreadgp,"Python");

		obj1.start();
		obj2.start();
		obj3.start();
		
		ThreadGroup cThreadgp = new ThreadGroup("Incubator");
		MyThread obj4 = new MyThread(cThreadgp,"Flutter");
		MyThread obj5 = new MyThread(cThreadgp,"ReactJs");
		MyThread obj6 = new MyThread(cThreadgp,"SpringBoot");

		obj4.start();
		obj5.start();
		obj6.start();
	}
}
