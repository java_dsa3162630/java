//run method is of parent class (ie Thread class) in overriding if parent class doesnt throws any eception then you cannot throw in child class
class MyThread extends Thread{

	public void run(){
	
		//Thread.sleep(1000);//unreported exception InterruptedException; must be caught or declared to be thrown
		try{
			Thread.sleep(1000);
		}catch(InterruptedException ie){
		
		}
		System.out.println(Thread.currentThread());//Thread-0 Thread
	}
}
class ThreadDemo{

	public static void main(String[] HP)throws InterruptedException{
	
		System.out.println(Thread.currentThread());//main thread
		
		MyThread obj = new MyThread();
		obj.start();
 	
		Thread.sleep(2000);// sleep method throws Interrupted Exception
		Thread.currentThread().setName("Core2Web");//Core2Web Thread  (u can change thread name)
		System.out.println(Thread.currentThread());
	}
}
