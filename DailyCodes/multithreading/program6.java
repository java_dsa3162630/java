class MyThread extends Thread{

	public void run(){
	
		Thread t = Thread.currentThread();
		System.out.println(t.getPriority());
	}
}
class ThreadDemo{

	public static void main(String[] HP){
	
		Thread t = Thread.currentThread();
		System.out.println(t.getPriority());

		MyThread obj = new MyThread();
		obj.start();

		t.setPriority(7);//priority shoild not be less than 0 or greater than 10
		
		MyThread obj1 = new MyThread();
		obj1.start();
		
	}
}
