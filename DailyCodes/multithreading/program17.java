class MyThread extends Thread{

	MyThread(ThreadGroup tg,String str){
	
		super(tg,str);
	}

	public void run(){
	
		System.out.println(Thread.currentThread());

		try{
			Thread.sleep(2000);
		}catch(InterruptedException ie){
		
			System.out.println(ie.toString());
		}
	}
}
class ThreadDemo{

	public static void main(String[] HP)throws InterruptedException{
	
		ThreadGroup PTG = new ThreadGroup("India");

		MyThread T1 = new MyThread(PTG,"Maharashtra");
		MyThread T2 = new MyThread(PTG,"Goa");
	
		T1.start();
		T2.start();
		
		ThreadGroup CTG = new ThreadGroup("Pakistan");
		
		MyThread T3 = new MyThread(CTG,"Karachi");
		MyThread T4 = new MyThread(CTG,"Lahore");

		T3.start();
		T4.start();

		ThreadGroup CTG2 = new ThreadGroup("Bangladesh");
		
		MyThread T5 = new MyThread(CTG2,"Karachi");
		MyThread T6 = new MyThread(CTG2,"Lahore");
		
		T5.start();
		T6.start();

		//CTG.interrupt();
		
		System.out.println(PTG.activeCount());
		System.out.println(PTG.activeGroupCount());

	}
}
