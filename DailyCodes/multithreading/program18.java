class MyThread implements Runnable{

	public void run(){
	
		System.out.println(Thread.currentThread());
		try{
			Thread.sleep(5000);
		}catch(InterruptedException ie){
		
			System.out.println(ie.toString());
		}
	}
}
class ThreadDemo{

	public static void main(String[] HP){
	
		ThreadGroup PTG  = new ThreadGroup("INDIA");

		MyThread obj1 = new MyThread();
		MyThread obj2 = new MyThread();

		Thread T1 = new Thread (PTG,obj1,"MAHARASHTRA");
		Thread T2 = new Thread (PTG,obj2,"GOA");

		T1.start();
		T2.start();
		
		ThreadGroup CTG  = new ThreadGroup("pakistan");

		MyThread obj3 = new MyThread();
		MyThread obj4 = new MyThread();

		Thread T3 = new Thread (CTG,obj3,"Karachi");
		Thread T4 = new Thread (CTG,obj4,"Lahore");

		T3.start();
		T4.start();

		System.out.println(PTG.activeCount());
		System.out.println(PTG.activeGroupCount());
	}
}
