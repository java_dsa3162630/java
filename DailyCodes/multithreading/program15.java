//ThreadGroup
class MyThread extends Thread{

	MyThread(ThreadGroup tg,String str){
	
		super(tg,str);
	}
	public void run(){
	
		System.out.println(Thread.currentThread());
	}
}
class ThreadDemo{

	public static void main(String[] HP){
	
		ThreadGroup pThreadgp = new ThreadGroup("C2W");
		MyThread obj1 = new MyThread(pThreadgp,"C");
		MyThread obj2 = new MyThread(pThreadgp,"Java");
		MyThread obj3 = new MyThread(pThreadgp,"Python");

		obj1.start();
		obj2.start();
		obj3.start();
	}
}
