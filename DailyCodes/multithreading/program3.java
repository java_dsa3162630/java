class MyThread extends Thread{

	public void run(){
	
		System.out.println("In Run");
		System.out.println(Thread.currentThread().getName());
	}
	public void start(){	//start method of thread class shound not be override 
	
		System.out.println("In Start");
		run();
	}
}
class ThreadDemo{

	public static void main(String[] HP){
		
		MyThread obj = new MyThread();//thread is created but it is not used
		obj.start();

		System.out.println(Thread.currentThread().getName());
	}
}
