class Demo extends Thread{

	public void run(){
		System.out.println("Demo :""+Thread.currentThread().getName());
	}
}
class MyThread extends Thread{

	public void run(){
		System.out.println("MyThread :"+Thread.currentThread().getName());
		Demo obj1 = new Demo();
		obj1.start();
	}
}
class ThreadDemo{
	public static void main(String[] HP){
		
		System.out.println("Main :"+Thread.currentThread().getName());

		MyThread obj = new MyThread();
		obj.start();
	}
}
