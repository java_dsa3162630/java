//concurrancy Methods in Thread class
//1.sleep() 2.Join() 3.yield()
//in this code we used sleep() and setName()
class MyThread extends Thread{

	public void run(){
	
		System.out.println(Thread.currentThread());//Thread-0 Thread
	}
}
class ThreadDemo{

	public static void main(String[] HP)throws InterruptedException{
	
		System.out.println(Thread.currentThread());//main thread
		
		MyThread obj = new MyThread();
		obj.start();
 	
		Thread.sleep(1000);// sleep method throws Interrupted Exception
		Thread.currentThread().setName("Core2Web");//Core2Web Thread  (u can change thread name)
		System.out.println(Thread.currentThread());
	}
}
