// Negation /complement

class Operators{

	public static void main(String[] args){

		int x = 2;
		int y = -10;

		System.out.println(~x);		//-3
		System.out.println(~y);		//9
		
	}
}
