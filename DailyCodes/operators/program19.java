//ternary operator
class Operstors{

	public static void main(String[] args){

		int x = 5;
		int y = 7;

		System.out.println((x<y) ? x : y);	//5
		System.out.println((x>y) ? x : y);	//7
	}
}
	
