// zero fill right shift bitwise operator
class Operators{

	public static void main(String[] args){

		int x = -7;

		System.out.println(x >>> 2);	//long integer
		System.out.println(x >> 2);	//-2
	}
}
