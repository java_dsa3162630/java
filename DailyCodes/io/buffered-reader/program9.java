import java.io.*;
class BufferDemo{

	public static void main(String[] args)throws IOException{

		InputStreamReader isr = new InputStreamReader(System.in);
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter String: ");
		String str = br.readLine();
		System.out.println("String is :"+str);

		br.close();// input stream closes from process to keyboard
			   // it cannot take input after this function and throws runtime exception.

		System.out.println("Enter Character: ");
		char ch = (char)isr.read();
		System.out.println("Character is :"+ch);
	}
}


