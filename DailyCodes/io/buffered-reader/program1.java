//INPUT using InputStreamReader 
//InputStreamReader can take only one character
import java.io.*;
class BufferDemo{

	public static void main(String[] args)throws IOException{

		InputStreamReader isr = new InputStreamReader(System.in);
		System.out.println("Enter a Character");
		//char ch = isr.read();// error :loosely conversion from int to char
				     // read method's return type is int
		char ch =(char)isr.read();
		System.out.println(ch);
	}
}

		

