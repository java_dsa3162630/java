import java.io.*;
class BufferDemo{
	public static void main(String[] args)throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter Socity Name: ");
		String SName = br.readLine();

		System.out.println("Enter Wing: ");
		char Wing = (char)br.read();// read because onlu one char
		br.skip(1); //A\n takes A ane \n still in pipe next goes to flats i/p
			    //br.skip(1); skips 1 letter

		System.out.println("Enter Flat NO: ");
		int FlatNo = Integer.parseInt(br.readLine());

		System.out.println("Socity Name: "+SName);	
		System.out.println("Wing: "+Wing);	
		System.out.println("Flat No: "+FlatNo);
	}
}	
