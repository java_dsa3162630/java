import java.io.*;
class BufferDemo{

	public static void main(String[] args)throws IOException{

		BufferedReader br1 = new BufferedReader(new InputStreamReader(System.in));
		BufferedReader br2 = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter Your Name :");
		String str1 = br1.readLine();

		br1.close();// it closes connection from process to keyboard later u cannot take i/p.
			    // run time exception

		System.out.println("Enter Your Surname :");
		String str2 = br2.readLine();

		System.out.println("Name: "+str1);
		System.out.println("Surname: "+str2);
	}
}


