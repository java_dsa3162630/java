import java.io.*;
class BufferedDemo{

	public static void main(String[] args)throws IOException{
		
		InputStreamReader isr = new InputStreamReader(System.in);
		BufferedReader br = new BufferedReader(isr);

		System.out.println("Enter Your name :");
		String name = br.readLine();
		
		/*System.out.println("Enter Your Age :");
		int age = br.readLine(); error string cannot convert to int
					 we need to use WRAPPER class
		*/
		System.out.println("Enter Your Age :");
		int age =Integer.parseInt(br.readLine());// Wrapper class
		System.out.println("Name is: "+name);
		System.out.println("Age is: "+age);
	}
}
