import java.io.*;
class IsrDemo{

	public static void main(String[] args)throws IOException{

		InputStreamReader isr = new InputStreamReader(System.in);

		System.out.println("Enter a Character");
		char ch =(char)isr.read();//take ip as "A\n"
		System.out.println(ch);// print A

		isr.close();   // CLOSE PIPELINE OF KEYBOARD(STREAM CLOSED)
		
		System.out.println("Enter a Character");//prints
		char ch1 =(char)isr.read();//read \n
		System.out.println(ch1);// print nothing goes to next line
	
		System.out.println("Enter a Character");//prints line
		char ch2 =(char)isr.read();//exception occur in run time because stream closed . next code will not execute
					   //
		System.out.println(ch2);
		
		System.out.println("Enter a Character");
		char ch3 =(char)isr.read();
	}
}
