import java.util.Scanner;
class ScannerDemo{

	public static void main(String[] args){

		Scanner obj = new Scanner(System.in);

		System.out.println("Enter your name : ");
		String name=obj.next();
		
		System.out.println("Enter your age : ");
		int age =obj.nextInt();

		System.out.println("Name :"+name);
		System.out.println("Age :"+age);
	}
}
