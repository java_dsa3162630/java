class Parent{
	Parent(){
		System.out.println("In Parent Constructor");
	}
	void parentProperty(){
		System.out.println("Flat,Car,Gold");
	}
}
class Child extends Parent{
	Child(){		// first line is invokspecial ie calls to parent constructor
		 System.out.println("In Child Constructor");
	}
}
class Client{
	
	public static void main(String[] HP){
	
		Child obj = new Child();
		obj.parentProperty();
	}
}
