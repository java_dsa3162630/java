class Parent{
	static{
		System.out.println("In Parents Static Block");
	}
}
class Child extends Parent{
	static{
		System.out.println("In Child Static Block");
	}
}
class Client{
	public static void main(String[] HP){
		Child obj = new Child();
	}
}
