class Unix{
	Unix(){
		System.out.println("UNIX Mother Of All OS");
	}
	void Base(){
		System.out.println("System Calls");
	}
}
class Linux extends Unix{
	Linux(){
		System.out.println("Linux-child of Unix");
	}
	void LinuxFounder(){
		System.out.println("Linus Torvalds");
	}
}
class Ubantu extends Linux{
	Ubantu(){
		System.out.println("Ubantu-child of Linux");
	}
	void UbantuFounder(){
		System.out.println("Mark shuttleWorth and team");
	}
}
class User{
	public static void main(String[] HP){
		Ubantu obj = new Ubantu();
		obj.Base();
		obj.LinuxFounder();
		obj.UbantuFounder();
	}
}
