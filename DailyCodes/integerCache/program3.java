class IntegerCacheDemo{

	public static void main(String[] HP){
	
		int arr1[] = {10,200,300};
		int arr2[] = {10,200,300};

		System.out.println(System.identityHashCode(arr1[0]));
		System.out.println(System.identityHashCode(arr1[1]));
		System.out.println(System.identityHashCode(arr1[2]));
		
		
		
		
		System.out.println(System.identityHashCode(arr2[0]));
		System.out.println(System.identityHashCode(arr2[1]));//1
		System.out.println(System.identityHashCode(arr2[2]));//2
		
		System.out.println(System.identityHashCode(arr2[0]));
		System.out.println(System.identityHashCode(arr2[1]));//1 new iHC
		System.out.println(System.identityHashCode(arr2[2]));//2 new iHC

		//iHC is used on objects but we are using it on primitive 
		//iHC internally calls valueof() method and it returns new Int thats why we get new iHC
	}
	
	
}

