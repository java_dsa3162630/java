//IntegerCache (autoboxing)

class IntegerCache{
	
	public static void main(String[] HP){
	
		int x = 10;	//primitive
		int y = 10;	//primitive
		Integer z = 10;		//wrapper class

		System.out.println(System.identityHashCode(x));//0x100
		System.out.println(System.identityHashCode(y));//0x100
		System.out.println(System.identityHashCode(z));//0x100
	}
}
