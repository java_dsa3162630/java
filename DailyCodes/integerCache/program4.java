class IntegerCacheDemo{

	public static void main(String[] HP){
	
		int arr1[] = {10,200,300};
		
		System.out.println(System.identityHashCode(arr1[0]));
		System.out.println(System.identityHashCode(arr1[1]));
		System.out.println(System.identityHashCode(arr1[2]));
		
		//int arr2[] = {10,200,300};
		Integer arr2[] = {10,200,300};// here Integer is a wrapper class iHC works on objects hence same iHC
		
		System.out.println(System.identityHashCode(arr2[0]));
		System.out.println(System.identityHashCode(arr2[1]));
		System.out.println(System.identityHashCode(arr2[2]));
		
		System.out.println(System.identityHashCode(arr2[0]));
		System.out.println(System.identityHashCode(arr2[1]));
		System.out.println(System.identityHashCode(arr2[2]));

		 
		
	}
	
	
}

