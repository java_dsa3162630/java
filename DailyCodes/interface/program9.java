interface Demo{

	static void fun(){
	
		System.out.println("DEMO-FUN");
	}
}
class Client {
	
	public static void main(String[] HP){
	
		Demo.fun();
	}
}
// static methods in interface you can call in another class using "interfaceName.methodName();"
