interface Demo{
	
	void fun();
	void gun();
}
abstract class Child1 implements Demo{

	public void fun(){
		System.out.println("In Fun");
	}
}
class Child2 extends Child1{

	public void gun(){
		System.out.println("In Gun");
	}
}
class Client{

	public static void main(String[] HP){
	
		Demo obj = new Child2();
		obj.fun();
		obj.gun();
	}
}

