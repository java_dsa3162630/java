interface Demo{

	default void fun(){
		System.out.println("Demo-fun");
	}
}
class Child implements Demo{

	public void fun(){
		System.out.println("Child-fun");
	}
}
class Client{
	
	public static void main(String[] HP){
	
		Demo obj = new Child();
		obj.fun();
	}
}
