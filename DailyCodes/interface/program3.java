//Multiple Inheritance in java

interface Demo1{

	void fun();
}
interface Demo2{

	void fun();
}
class Child implements Demo1,Demo2{

	public void fun(){
		System.out.println("IN FUN");
	}
}
class Client{

	public static void main(String[] HP){
	
		Demo1 obj = new Child();
		obj.fun();
		
		Demo2 obj2 = new Child();
		obj2.fun();
	}
}
