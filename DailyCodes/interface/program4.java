interface Demo1{

	static void m1(){
		System.out.println("Demo1-m1");
	}
}
interface Demo2{

	static void m2(){
		System.out.println("Demo2-m2");
	}
}
class Client{

	public static void main(String[] HP){
	
		Demo1.m1();
		Demo2.m2();
	}
}
//in interface methods are static (ie they are interface method) you cannot create object 
//hence you need call as interfacename.methodname();
