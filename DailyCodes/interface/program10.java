interface Demo{

	static void fun(){
		
		System.out.println("Demo-Fun");	
	}
}
class Child implements Demo{

	void fun(){
	
		System.out.println("Child-Fun");
	}
}
//this is not overriding in demo method is static that will not override in child and 
//in child fun is its own method
