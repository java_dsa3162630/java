interface Demo{ //interface methods are by default public and abstract

	void fun();
	void gun();
}
class Child implements Demo{

	public void fun(){ //by default methods are default but in case of interface you need to public the method otherwise gives error of weak access specifier previliage
		System.out.println("In Fun");
	}
	public void gun(){
		System.out.println("In gun");
	}
}
class Client{

	public static void main(String[] HP){
	
		Demo obj = new Child();
		obj.fun();
		obj.gun();
	}
}
