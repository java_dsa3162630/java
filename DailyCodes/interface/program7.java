interface Demo1{

	default void fun(){
	
		System.out.println("In Demo1-fun");
	}
}
interface Demo2{

	default void fun(){
	
		System.out.println("In Demo2-fun");
	}
}
class Child implements Demo1,Demo2{

	public void fun(){
		
		System.out.println("In Child-fun");
	}
}
class Client{

	public static void main(String[] HP){
	
		Demo1 obj = new Child();
		obj.fun();
		
		Demo2 obj2 = new Child();
		obj2.fun();

		Child obj3 = new Child();
		obj3.fun();
	}
}
//if you are using default methods in interface then you must override those methods in child class ("otherwise error of ambiguity")
