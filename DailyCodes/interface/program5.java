interface Demo{

	static void fun(){	//public static void fun
		System.out.println("Fun");
	} 
	default void gun(){	//public default void gun
		System.out.println("Gun");
	}
}
//in interface default keyword is treated as modifier(not access-specifier)
//in interface static method cannot override in child class
class Child implements Demo{


}
class Client{

	public static void main(String[] HP){
	
		Child obj = new Child();
		obj.gun();
		Demo.fun();
		
	}
}
