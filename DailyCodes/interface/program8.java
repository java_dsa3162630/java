interface Demo{

	static void fun(){
	
		System.out.println("In Demo-fun");
	}
}
class Child implements Demo{

}
class Client{

	public static void main(String[] HP){
	
		Child obj = new Child();
		//obj.fun(); error:cannot find symbol("Static doesnt override in child class")

		Demo.fun();//you can call static methods in interface using interfaceName.MethodName();
	}
}

/* Aboves Same Code In Class
 
class  Demo{

        static void fun(){

                System.out.println("In Demo-fun");
        }
}
class Child extends Demo{
	//here fun method gets inherited in child class
}
class Client{

        public static void main(String[] HP){

                Child obj = new Child();
                obj.fun(); //In Demo-fun
	}
}
*/

