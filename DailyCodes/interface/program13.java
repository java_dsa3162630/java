interface Demo{

	int x = 10;
	void fun();
}

class DemoChild implements Demo{

	public void fun(){
		int x = 30;
		System.out.println(x);
	}
}

class Client{

	public static void main(String[] HP){
	
		Demo obj = new DemoChild();
		obj.fun();
	}
}
