/* priority as
 * static variable
 * static block 
 * static method 
 * instance variable 
 * instance block 
 * constructor
 * instance method
 */
class Demo{

	int x = 10;
	static int y = 20;

	Demo(){
		System.out.println("In constructor");
	}

	static{
		System.out.println("In static block 1");
	}

	{
		System.out.println("In instance block 1");
	}
	
	public static void main(String[] HP){
	
		Demo obj = new Demo();
	}

	static{
		System.out.println("In static block 2");
	}

	{
		System.out.println("In instance block 2");
	}
}
