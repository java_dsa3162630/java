//Instance Method

class Demo{

	int x = 10;
	static int y = 20;

	//instance method
	void fun(){
	
		System.out.println(x);
		System.out.println(y);
	}

	public static void main(String[] HP){
	
		Demo obj = new Demo();
		obj.fun();
	}
}
