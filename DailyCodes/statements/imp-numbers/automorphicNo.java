// Automorphic Number - the number whose last digits of its square number are same as that number

class Automorphic{

        public static void main(String[] args){

                int N = 20;
                int sq = N*N;
		int temp = 0;
                int count = 0;

                while(N != 0){
			count++;
			int rem1 = N%10;
			int rem2 = sq%10;

			if(rem1 == rem2)
				temp++;

			N = N/10;
			sq = sq/10;
                }
                if(count == temp)
                        System.out.println("Automorphic Number");
                else
                        System.out.println("Not Automorphic Number");
        }
}

