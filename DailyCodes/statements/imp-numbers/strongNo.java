// Strong Number - the addition of factorial of all the digits in the number is the number itself

class Strong{

        public static void main(String[] args){

                int N = 145;
		int temp = N;
                int sum = 0;

                while(temp != 0){
			int rem = temp%10;

			int fact = 1;
			for(int i=1; i<=rem; i++)
				fact = fact * i;

			sum = sum + fact;

			temp = temp/10;
                }
                if(sum == N)
                        System.out.println(N+" is Strong Number");
		else
                        System.out.println(N+" is Not Strong Number");
        }
}


