class SwitchDemo{

	public static void main(String[] args){

		int x = 5;

		switch(x){

			// In Switch duplicate cases(same cases) are not allowed
			case 1:
				System.out.println("One");
				break;
			case 2:
				System.out.println("First-2");
				break;
			case 5:
				System.out.println("First-5");
				break;
			case 5:
				System.out.println("Second-5");
				break;
			case 2:
				System.out.println("Second-2");
				break;
			default:
				System.out.println("No match");
				break;
		}
		System.out.println("After-Switch");
	}
}

