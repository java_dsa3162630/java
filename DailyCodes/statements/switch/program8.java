class Statement{

	public static void main(String[] args){

		int x=3;

		switch(x){
			//Default has least priority
			default:
				System.out.println("Default");
				break;
			case 1:
				System.out.println("One");
				break;
			case 2:
				System.out.println("Two");
				break;
			case 3:
				System.out.println("Three");
				break;
		
		}
		System.out.println("After-Switch");
	}
}
