class NestedSwitchDemo{

        public static void main(String[] arg){

                String bookCat = "Self-help";

                System.out.println("Book's Categories");

                switch(bookCat){

                        case "Fictional":{

                                           String str1 = "Mystery";

                                           switch(str1){

                                                   case "Mystery":
                                                           System.out.println("The Big Sleep");
                                                           break;

                                                   case "Thriller":
                                                           System.out.println("The Couple Next Door");
                                                           break;
                                           }
                        }
                        break;

                        case "Self-help":{

                                           String str1 = "Personal";

                                           switch(str1){

                                                   case "Finance":
                                                           System.out.println("The Psychology Of Money");
                                                           break;

                                                   case "Personal":
                                                           System.out.println("The Courage To Be Disliked");
                                                           break;
					   }

                        }
                        break;
                }
                System.out.println("After Switch");
        }
}                        

