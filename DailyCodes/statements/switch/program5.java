class SwitchDemo{

        public static void main(String[] args){

                String str = "Mon";

                switch(str){

                        // switch does not allow strings in the versions lower than 1.6, from version 1.7 and onwards strings are legal to use in switch.
                        case "Mon":
                                System.out.println("Monday");
				break;
                        case "Tue":
                                System.out.println("Tuesday");
				break;
                        case "Wed":
                                System.out.println("Wednesday");
				break;
                        default:
                                System.out.println("No match");
				break;
                }
        }
}

