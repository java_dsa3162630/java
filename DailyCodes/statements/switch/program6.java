// Nested Switch-cases

class NestedSwitchDemo{

	public static void main(String[] arg){
	
		String str = "veg";

		System.out.println("Oh Pune");

		switch(str){
			
			case "veg":{
			
					   String str1 = "starter";

					   switch(str1){
					   
						   case "starter":
							   System.out.println("Fries");
							   break;
						   
						   case "main course":
							   System.out.println("Paneer Tikka");
							   break;
					   }
			}
			break;

			case "non-veg":{
					       
					   String str1 = "main course";

					   switch(str1){
					   
						   case "starter":
							   System.out.println("Soup");
							   break;
						   
						   case "main course":
							   System.out.println("Chicken Curry");
							   break;
					   }
			
			}
			break;
		}
		System.out.println("After Switch");
	}
}

