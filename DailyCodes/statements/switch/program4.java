class SwitchDemo{

        public static void main(String[] args){

                int x = 3;
		int a = 1, b = 2;

                switch(x){

                        // values at case must be static, variables are not allowed.
                        case a:
                                System.out.println("One");
				break;
                        case b:
                                System.out.println("Two");
				break;
                        case a+b:
                                System.out.println("Three");
				break;
                        case a+a+b:
                                System.out.println("Four");
				break;
                        case a+b+b:
                                System.out.println("Five");
				break;
                        default:
                                System.out.println("No match");
				break;
                }
                System.out.println("After switch");
        }
}


