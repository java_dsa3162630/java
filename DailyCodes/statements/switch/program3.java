class SwitchDemo{

	public static void main(String[] args){

		int ch='A';

		switch(ch){
			//characters are considered as integer (ASCII)internally
			//error:duplicate case lable
			case 'A':
				System.out.println("char-A");
				break;
			case 65:
				System.out.println("NUM-65");
				break;
			case 'b':
				System.out.println("Char-'B'");
				break;
			case 66:
				System.out.println("NUM-66");
				break;
			default:
				System.out.println("No-Match");
				break;
		}
		System.out.println("After Switch");
	}
}
