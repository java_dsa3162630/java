// Given an integer N, print its all digits
// i/p: 6531
// o/p: 1
// 	3
// 	5
// 	6
class Statement{

	public static void main(String[] args){

		int n = 6531;

		while(n != 0){
			System.out.println(n%10);
			n=n/10;
		}
	}
}
