// Q.3) Take an integer N as input and print odd intgers from 1 to N using loop
class Statements{
	
	public static void main(String[] args){

		int n=10;
		int i=1;
		while(i<=n){
			if(i%2!=0){
				System.out.println(i);
				
			}
				i++;
		}
	}
}
