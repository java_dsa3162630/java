/*
Take an integer N as input. Print perfect squares till N.
Perfect Squares: An integer whose square root is a integer.
25 -> 5 : yes
81 -> 9 : yes
1 -> 1  : yes
10 -> 3.13 : no

i/p: 30
o/p: 1 4 9 16 25
*/

// Sum of digits of given number

class Statements{

        public static void main(String[] args){

                int N = 30;
		int i=1;

                while(i*i <= 30){
                        System.out.print(i*i+" ");
			i++;
                }
		System.out.println();
        }
}

