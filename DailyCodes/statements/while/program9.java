//reverse number

/*class Statement{

	public static void main(String[] args){

		int n=6531;
		int rev;
		while(n!=0){
			rev=n%10;
			System.out.print(+rev);
			n=n/10;
		}
	}
}*/

class Statements{

	public static void main(String[] args){

		int n= 6531;
	        int rev = 0;

		while(n != 0){
			int rem = n%10;
			rev=rev*10+rem;
			n=n/10;
		}
		System.out.println(rev);

	}
}
