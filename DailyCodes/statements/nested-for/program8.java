/*
        1	2	3
	4	5	6
	7	8	9
*/

// using single loop

class Pattern{

        public static void main(String[] args){

		int n = 1;
		int N = 3;

                for(int i=1; i<=N*N; i++){
                        
			System.out.print(n+"\t");
			n++;

			if(i%N == 0)
				System.out.println();
                }
        }
}


