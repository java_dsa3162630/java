/*
        1	2	3
	1	2	3
	1	2	3
*/

class Pattern{

        public static void main(String[] args){

		int n = 3;
		int num = 1;

                for(int i=1; i<=n*n; i++){
                        System.out.print(num+"\t");
			num++;
			
			if(i%n == 0)
				System.out.println();
			if(num>3)
				num = 1;
                }
        }
}

