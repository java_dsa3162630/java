// 5. Given an integer age as input, print whether its odd or even.
// Input: 7
// Output: 7 is ODD
// Input: 4
// Output: 4 is EVEN

class Statements{

        public static void main(String[] args){

                int x = 7;

                if(x%2 == 0)
                        System.out.println(x + " is Even");
		else
                        System.out.println(x + " is Odd");
        }
}
