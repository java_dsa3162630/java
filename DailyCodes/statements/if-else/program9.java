// 7. Given an integer input which represents units of electricity and print consumed at your house.
// Calculate and print the bill amount.
// units <= 100		price/unit is 1
// units > 100		price/unit is 2
// Input: 50
// Output: 50
// Input: 200
// Output: 300

class Statements{

        public static void main(String[] args){

                int units = 200;

                if(units <= 100)
                        System.out.println(units);
		else
                        System.out.println(units*2-100);
        }
}

