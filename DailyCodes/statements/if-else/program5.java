// 3. Given the temperature of a person in farenheit, print whether the person has HIGH, NORMAL, LOW temperature.
// > 98.6			-HIGH
// 98.0 <= and <= 98.6		-NORMAL
// < 98.0			-LOW

class Statements{

	public static void main(String[] args){

		float temp = 98.4f;

		if(temp < 98.0)
			System.out.println("Low");
		else if(temp > 98.6)
			System.out.println("High");
		else 
			System.out.println("Normal");
	}
}

