// if-else ladder

class Statements{

	public static void main(String[] args){

		int x = 52, y = 53;

		if(x > y)
			System.out.println(x+ " is greater");
		else if(x == y)
			System.out.println("Both are equal");
		else
			System.out.println(y+" is greater");
	}
}
