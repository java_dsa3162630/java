// 4. Take an integer as input and print whether it is divisible by 4 or not.
// Input: 5
// Output: Not divisible

class Statements{
	
	public static void main(String[] args){

		int x = 5;

		if(x % 4 == 0)
			System.out.println("Divisible");
		else 
			System.out.println("Not Divisible");
	}
}


