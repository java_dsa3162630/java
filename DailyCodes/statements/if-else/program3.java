// 2. Take two integers A & B as input, print the max of two.
// Assume A and B are not equal
// Input: A = 5, B = 7
// Output: 7 is greater

class Statements{

	public static void main(String[] args){

		int a = 5;
	        int b = 7;

		if(a > b)
			System.out.println( a+ " is greater");
		else
			System.out.println( b+ " is greater");
	}
}
