// 6. Given an integer value as input,
// print FIZZ if it is divisible by 3.
// print BUZZ if it is divisible by 5.
// print FIZZ-BUZZ if it is divisible by both.
// If not then print "Not divisible by both"

class Statements{

        public static void main(String[] args){

                int x = 15;

                if(x % 3 == 0 && x % 5 == 0)
                        System.out.println("FIZZ-BUSS");
		else if(x % 3 == 0)
                        System.out.println("FIZZ");
		else if(x % 5 == 0)
                        System.out.println("BUSS");
		else
                        System.out.println("Not divisible by both");
        }
}

