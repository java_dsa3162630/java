class Demo{

	void fun(float x){
		
		System.out.println("In fun");
		System.out.println(x);
	}
	
	public static void main(String[] args){
		
		Demo obj = new Demo();

		obj.fun(true);//error:incompatible types boolean cannot converted to float
	}
}
