class MethodDemo{
	
	int x = 10;
	static int y = 20;

	void fun(){ 		
		System.out.println(x);//10
		System.out.println(y);//20
	}
	
	public static void main(String[] args){
		MethodDemo obj = new MethodDemo();
		obj.fun();
	}
}
