class Demo{

	void fun(int x){
	
		System.out.println(x);
	}
	public static void main(String[] args){
		
		System.out.println("In main");

		Demo obj = new Demo();

		obj.fun();//error:parameter should be given coz arguments are there in fun

		System.out.println("End Main");
	}
}
