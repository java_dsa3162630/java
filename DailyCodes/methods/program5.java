import java.io.InputStreamReader;
import java.io.BufferedReader;
import java.io.IOException;

class Operations{

	static void add(int a,int b){
	
		int ans = a+b;
		System.out.println("Addition is ="+ans);
	}
	
	static void sub(int a,int b){
		
		int ans = a-b;
		System.out.println("Substraction is ="+ans);
	}

	static void mul(int a,int b){

		int ans = a*b;
		System.out.println("Multiplication is="+ans);
	}

	static void div(int a,int b){

		int ans = a/b;
		System.out.println("Division is ="+ans);
	}

	public static void main(String[] args) throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter Two Entegers");
		int a = Integer.parseInt(br.readLine());
		int b = Integer.parseInt(br.readLine());

		add(a,b);
		sub(a,b);
		mul(a,b);
		div(a,b);
	}
}
