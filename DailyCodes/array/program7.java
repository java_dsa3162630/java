//Even Odd Count In An Array

import java.io.*;

class EvenOddCount{
	
	public static void main(String[] HP)throws IOException{
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		System.out.println("Enter Array Size");

		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];

		int EvenCount = 0;
		int OddCount = 0;

		System.out.println("Enter Array Elements");
		
		for(int i=0; i<arr.length; i++){
			arr[i]=Integer.parseInt(br.readLine());

			if(arr[i]%2==0){
				EvenCount++;
			}else{
				OddCount++;
			}
		}
		System.out.println("Even Element Count In Array is "+EvenCount);
		System.out.println("Odd Element Count In Array is "+OddCount);
		
	}
}
