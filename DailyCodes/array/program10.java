class IdentityHashCode{

	public static void main(String[] HP){
	
		int arr[] = {10,200};
		int arr1[] = {10,200};

		System.out.println(arr);//different IHC
		System.out.println(arr1);//different IHC
					 
		System.out.println(System.identityHashCode(arr));//different IHC
		System.out.println(System.identityHashCode(arr1));//different IHC
		
		System.out.println(System.identityHashCode(arr[0]));//same IHC coz data in range from -128 to 127
		System.out.println(System.identityHashCode(arr1[0]));// same IHC coz data in range from -128 to 127
		
		System.out.println(System.identityHashCode(arr[1])); //different IHC
		System.out.println(System.identityHashCode(arr1[1]));//different IHC
	}
}
