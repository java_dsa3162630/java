class ArrayDemo{

	void fun(int[] arr){
	
		arr[1]=70;
		arr[2]=80;
	}
	
	public static void main(String[] HP){
	
		int arr[]={10,20,30,40};

		System.out.println(System.identityHashCode(arr[0]));
		System.out.println(System.identityHashCode(arr[1]));
		System.out.println(System.identityHashCode(arr[2]));
		System.out.println(System.identityHashCode(arr[3]));

		ArrayDemo obj = new ArrayDemo();
		obj.fun(arr);

		for(int x : arr){
			System.out.println(x);
		}

		System.out.println(System.identityHashCode(arr[1]));//SAME 1
		System.out.println(System.identityHashCode(arr[2]));//SAME 2
		
		int x = 70;
		int y = 80;

		System.out.println(System.identityHashCode(x));//SAME 1
		System.out.println(System.identityHashCode(y));//SAME 2
							       //VALUE IS BTWN -128 TO 127
	}

}
