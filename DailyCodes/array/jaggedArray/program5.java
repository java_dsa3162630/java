import java.io.*;

class JaggedArray{

        public static void main(String[] args) throws IOException {

                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

                System.out.print("Enter no. of rows: ");
		int row = Integer.parseInt(br.readLine());

                int arr[][] = new int[row][];

                for(int i=0; i<arr.length; i++){

                	System.out.print("Enter no. of columns in row "+i+" : ");
			int col = Integer.parseInt(br.readLine());
                	arr[i] = new int[col];
                	System.out.println("Enter array elements");
                        
			for(int j=0; j<arr[i].length; j++)
                                arr[i][j] = Integer.parseInt(br.readLine());
                }
                System.out.println();
                
		System.out.println("Jagged Array:");
                for(int x[] : arr){
                        for(int y : x)
                                System.out.print(y+" ");
                        System.out.println();
                }
        }
}
