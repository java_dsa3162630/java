class JaggedArray{
	
	public static void main(String[] HP){
	
		int arr[][] = {{1,2,3},{4,5},{7}};

		for(int x[] : arr){
			for(int y : x){
				System.out.print(y+" ");
			}
			System.out.println();
		}
	}
}
