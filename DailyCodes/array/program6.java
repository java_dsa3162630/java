//take size and array elements from user and print their sum using scanner class

import java.util.*;

class ArraySum{

		public static void main(String[] HP){
		
			Scanner sc = new Scanner(System.in);
			
			System.out.println("Enter Array size");
			
			int size = sc.nextInt();
			
			int arr[] = new int[size];
			
			int sum=0;
			
			System.out.println("Enter Array Elements");
			
			for(int i=0; i<size; i++){
				arr[i]=sc.nextInt();
				sum=sum+arr[i];
			}
			
			System.out.println("Sum of array elements is: "+sum);
		}
}
