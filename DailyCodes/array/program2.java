class ArrayDemo{

	public static void main(String[] hp){
		
		int arr1[] = new int[4];

		arr1[0] = 10;
		arr1[1] = 20;
		arr1[2] = 30;
		arr1[3] = 40;

		int arr2[] = {10,20,30,40};

		int arr3[] = new int[]{10,20,30,40};

		int arr4[] = new int[4]{10,20,30,40};//erroe:give only one dimension
	}
}
