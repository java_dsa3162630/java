class ArrayDemo{
	
	public static void main(String[] hp){
		
		int arr1[] = {10,20,30,40};

		float arr2[] = {1.1f,1.2f,1.3f,1.4f};

		char arr3[] = {'A','B','C','D'};

		boolean arr4[] = {true,false,true};

		System.out.println("Integer values");
		System.out.println(arr1[0]);	
		System.out.println(arr1[1]);	
		System.out.println(arr1[2]);	
		System.out.println(arr1[3]);	
		
		System.out.println("Float values");
		System.out.println(arr2[0]);	
		System.out.println(arr2[1]);	
		System.out.println(arr2[2]);	
		System.out.println(arr2[3]);	
		
		System.out.println("Char Values");
		System.out.println(arr3[0]);
		System.out.println(arr3[1]);
		System.out.println(arr3[2]);
		System.out.println(arr3[3]);
		
		System.out.println("Boolean Values");
		System.out.println(arr4[0]);
		System.out.println(arr4[1]);
		System.out.println(arr4[2]);
	}

}
