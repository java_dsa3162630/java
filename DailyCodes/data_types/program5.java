class Demo{

	int x = 10;

	public static void main(String[] args){
		
		int y = 20;

		System.out.println(x);	//instance variable gets actual memory space when object of an class is created
					//we can't access the value of instance variable without setting it as "static" or creating class object  	
		
		System.out.println(y);
	}
}	

	
