class Demo{

	public static void main(String[] args){

		byte var1 = 18;
		byte var2 = 18;
		
		System.out.println(var1);	//18
		System.out.println(var2);	//18     

		var1 = var1 + var2;	//error:when two 'byte' type of variables are added on ALU they are by default stored as 'int'in other variable
		
		System.out.println(var1);
		System.out.println(var1);
	}
}

