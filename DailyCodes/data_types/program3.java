class Demo{

	public static void main(String[] args){

		//float f1 = 7.5;   //in java float numbers must be written in java
		//float f1 = 7.5;
		
		float f1 = 7.5f;
		float f2 = 10.25f;

		System.out.println(f1);
		System.out.println(f2);
	}
}
