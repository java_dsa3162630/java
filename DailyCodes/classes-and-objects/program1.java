class Core2Web{

	Core2Web(){			//Constructor won't get called until class' object in not created 
		
		System.out.println("In Core2Web Constructor");
	}
	
	int x = 10;

	public static void main(String[] HP){
	
		System.out.println("In Main");
		
		Core2Web obj = new Core2Web();//implicitly call to constructor we don't need to call
		
		System.out.println(obj.x);
		System.out.println(obj.y);
		
	}

	int y = 20;


}
