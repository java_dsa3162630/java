//static block
//static block executes before main() method

class StaticBlockDemo{
	
	static{
		System.out.println("In static block");
	}
	
	public static void main(String[] HP){
	
		System.out.println("In Main Method");
	}
}
