
class Demo{

	static int x = 10;

	static{
	
		static int y = 20;	//error (static variables cant be initilized in any block except class)
	}

	void fun(){
	
		static int z = 20;	//error (static variables cant be initilized in any block except class)
	}

	static void gun(){
	
		static int a = 20;	//error (static variables cant be initilized in any block except class)
	}
}
