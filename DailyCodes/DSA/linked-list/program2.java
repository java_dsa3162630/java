import java.util.*;

class Node{
	
	Node prev = null;
	int data;
	Node next = null;
	
	Node(int data){
		this.data = data;
	}
}

class DoublyLinkedList{

	Node head = null;

	int countNode(){

		int count = 0;

		if(head == null){
			return 0;
		}else{
			Node temp = head;
			
			while(temp != null){
				count++;
				temp = temp.next;
			}
			return count;
			
		}
	
	}

	void printDLL(){
	
		if(head == null){
			System.out.println("Doubly LinkedList is Empty.....!");
		}else{
			Node temp = head;

			while(temp.next != null){
			
				System.out.print(temp.data + "->");
				temp = temp.next;
			}
				System.out.println(temp.data);
		}
	}

	void addFirst(int data){
		
		Node newNode = new Node(data);

		if(head == null){
			head=newNode;
		}else{
			newNode.next = head;
			head.prev = newNode;
			head = newNode;
		}
	}

	void addLast(int data){
	
		Node newNode = new Node(data);

		if(head == null){
		
			head = newNode;
		}else{
			
			Node temp = head;

			while(temp.next != null){
			
				temp = temp.next;
		}
			temp.next = newNode;
			newNode.prev = temp; 
		}
	}

	void addAtPos(int pos, int data){
		
		if(pos <=0 || pos >= countNode()+2){
		
			System.out.println("Wrong Input");
			return;
		}

		if(pos == 1){
			addFirst(data);
		}else if(pos == countNode()+1){
			addLast(data);
		}else{
	
			Node newNode = new Node(data);
		
			Node temp = head;

			while(pos - 2 != 0){
			
				temp = temp.next;
				pos--;
			}
			newNode.prev = temp;
			newNode.next = temp.next;
			temp.next = newNode;
			newNode.next.prev = newNode;

		}
	}

	void deleteFirst(){
		
		if(head == null){
			System.out.println("Empty LinkedList");
		}else if (countNode() == 1){
			head = null; 
		}else{
			head = head.next;
			head.prev = null;
		}
	}

	void deleteLast(){
		
		if(head == null){
			System.out.println("Empty LinkedList");
		}else if (countNode() == 1){
			head = null; 
		}else{
			Node temp = head;

			while(temp.next != null){
			
				temp = temp.next;
			}
			temp.prev.next = null;
			temp.prev = null;
		}
	}
	

	void deleteAtPos(int pos){
	
		if(pos <=0 || pos >= countNode()+1){
		
			System.out.println("Wrong Input");
			return;
		}

		if(pos == 1){
			deleteFirst();
		}else if(pos == countNode()){
			deleteLast();
		}else{
			Node temp = head;

			while(pos-2 != 0){
				temp = temp.next;
				pos--;
			}

			temp.next = temp.next.next;
			temp.next.prev = temp;
		}
	}
}

class Client{

	public static void main(String[] args){
	
		DoublyLinkedList Dll = new DoublyLinkedList();
		Scanner sc = new Scanner(System.in);

		char ch;

		do{
			System.out.println("1.addFirst");
			System.out.println("2.addLast");
			System.out.println("3.addAtPos");
			System.out.println("4.deleteFirst");
			System.out.println("5.deleteLast");
			System.out.println("6.deleteAtPos");
			System.out.println("7.countNodes");
			System.out.println("8.printDLL");
			
			System.out.print("Enter your choice: ");
			int choice = sc.nextInt();

			switch(choice){
			
				case 1:
					{
						System.out.print("Enter data in node: ");
						int data = sc.nextInt();
						Dll.addFirst(data);
					}
					break;

				case 2:
					{
						System.out.print("Enter data in node: ");
						int data = sc.nextInt();
						Dll.addLast(data);
					}
					break;

				case 3:
					{
						System.out.print("Enter data in node: ");
						int data = sc.nextInt();
						System.out.print("Enter position of node: ");
						int pos = sc.nextInt();
						Dll.addAtPos(pos,data);
					}
					break;

				case 4:
					Dll.deleteFirst();
					break;
					
				case 5:
					Dll.deleteLast();
					break;
					
				case 6:
					{
						System.out.print("Enter position of node: ");
						int pos = sc.nextInt();
						Dll.deleteAtPos(pos);
					}
					break;

				case 7:
					System.out.println("Node Count = "+Dll.countNode());
					break;

				case 8:
					Dll.printDLL();
					break;

				default:
					System.out.println("Invalid Input!");
					break;
			}

			System.out.println("Continue?");
			ch = sc.next().charAt(0);

		}while(ch == 'Y' || ch == 'y');
	}
}
