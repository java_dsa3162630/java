// REVERSE LINKEDLIST 
// 1.using Iterator - Inplace reverse
// 2.using recurssion -Inplace reverse
import java.util.*;
class Node{

	int data;
	Node next = null;

	Node(int data){
	
		this.data = data;
	}
}

class LinkedList{

	Node head = null;

	//addNode
	void addNode(int data){
		
		Node newNode = new Node(data);
		if(head == null){
		
			head =newNode;
		}else{
		
			Node temp = head;

			while(temp.next != null){
			
				temp=temp.next;
			}
			temp.next = newNode;
		}
	}
	//printLL
	void printLL(){
	
		if(head == null){
		
			System.out.println("Empty Linkedlist");
			return;
		}else{
		
			Node temp = head;
			while(temp.next != null){
			
				System.out.print(temp.data + "->");
				temp = temp.next;
			}
			System.out.println(temp.data);
		}
	}
	//reverseItr
	void reverseItr(){

		if(head == null)
			return;
		Node prev = null;
		Node curr = head;
		Node forward = null;

		while(curr != null){
		
			forward = curr.next;
			curr.next = prev;
			prev = curr;
			curr = forward;
		}
		head = prev;
	}

	//reverseRec
	void reverseRec(Node prev, Node curr){
	
		if(curr == null){
		
			head = prev;
			return;
		}else{
			Node forward = curr.next;
			curr.next = prev;
			prev = curr;
			curr = forward;
		
		}
		reverseRec(prev,curr);
	}

}

class Client{

	public static void main(String[] Args){
	
		LinkedList ll = new LinkedList();
	
		Scanner sc = new Scanner(System.in);
		char ch;
		
		do{
	
			System.out.println("1.addNode");
			System.out.println("2.printLL");
			System.out.println("3.reverseItr");
			System.out.println("4.reverseRec");
			
			System.out.println("Enter Your Choice");
			int choice = sc.nextInt();

			switch(choice){
			
				case 1 :
					{
					
						System.out.println("Enter Data");
						int data = sc.nextInt();
						ll.addNode(data);
					}
					break;
				case 2:
					ll.printLL();
					break;
				case 3:
					ll.reverseItr();
					break;
				case 4:
					{
						Node prev = null;
						ll.reverseRec(prev,ll.head);
					}
					break;
				default:
					System.out.println("Wrong choice");
					break;
			}
			System.out.println("Do You Want To Continue?");
			ch = sc.next().charAt(0);
		}
		while(ch == 'Y' || ch == 'y');

	}
}
