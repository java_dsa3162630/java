
class Demo{

	int x = 10;

	Demo(){
		System.out.println("In no-args constructor");
	}

	Demo(int x){
		this();	//Demo();
		System.out.println("In para constructor");
	}

	public static void main(String[] HP){
	
		Demo obj = new Demo(100);	//Demo(obj,100);
	}
}
