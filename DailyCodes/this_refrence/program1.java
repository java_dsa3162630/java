
class Demo{

	int x = 10;

	Demo(){		//internally --> Demo(Demo this){}
		System.out.println("In no-args constructor");
	}
	
	Demo(int x){	//internally --> Demo(Demo this, int x){}
		System.out.println("In para constructor");
	}

	public static void main(String[] HP){
	
		Demo obj1 = new Demo();
		//constructor call --> Demo(obj1);
		Demo obj2 = new Demo(10);
		//constructor call --> Demo(obj2,10);
	}
}
