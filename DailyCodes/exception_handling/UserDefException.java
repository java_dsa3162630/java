//USER DEFINED EXCEPTION
import java.util.Scanner;

class DataOverFlowException extends RuntimeException{

	DataOverFlowException(String msg){
	
		super(msg);
	}
}
class DataUnderFlowException extends RuntimeException{

	DataUnderFlowException(String msg){
	
		super(msg);
	}
}
class ArrayDemo{

	public static void main(String[] HP){
		
		int arr[] = new int[5];
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter Integer Values");
		System.out.println("Note : Data Should be < 100 and >0");

		for(int i = 0; i<arr.length; i++){
			int data = sc.nextInt();

			if(data<0)
				throw new DataUnderFlowException("Data Lahan Aahe Zero Peksha");
			if(data>100)
				throw new DataOverFlowException("Data Motha Aahe 100 Peksha");
			arr[i]=data;
		}
		System.out.println("YOUR DATA IS ");
		for(int i=0; i<arr.length; i++){
			System.out.println(arr[i]);
		}


	}
}
