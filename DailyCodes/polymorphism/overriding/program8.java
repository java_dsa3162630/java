//co-varient return type

class Parent{

	Object fun(){
		System.out.println("Parent fun");
		return new Object();
	}
}
class Child extends Parent{
	
	String fun(){
		System.out.println("Child Fun");
		return "String";
	}
}
class Client{
	
	public static void main(String[] HP){
	
		Parent p = new Child();
	        p.fun();	
	}
}
