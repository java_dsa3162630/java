// static modifier
class Parent{

	static void fun(){
		System.out.println("Parent fun");
	}
}
class Child extends Parent{

	static void fun(){
		System.out.println("Child fun");
	}
}
class Client{
	
	public static void main(String[] HP){
	
		Child obj = new Child();
		obj.fun();

		Parent obj1 = new Parent();
		obj1.fun();

		Parent obj2 = new Child();
		obj2.fun();
	}
}
/* in both class fun method is static  hence this is not the case of overriding ,both methods treated unique and different
 * this is called method hiding
 * only those methods will get called whose class refrence is given at a compile time */
