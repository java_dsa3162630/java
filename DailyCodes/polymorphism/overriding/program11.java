//final modifier
class Parent{
	final void fun(){   //final method cannot be override
		System.out.println("Parent fun");
	}
}
class Child extends Parent{

	void fun(){ //erroe fun in child cannot override fun in parent(overriden method is final)
		System.out.println("Child fun");
	}
}
class Client{

	public static void main(String[] HP){
	
		Parent obj = new Child();
	        obj.fun();	
	}
}
//if a parent class want the child should not override the method then make the method final in parent class itself .so that child cannot override
