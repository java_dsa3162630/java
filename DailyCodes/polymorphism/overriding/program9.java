//access specifier in overriding

class Parent{
	public void fun(){
		System.out.println("In Parent fun");
	}
}
class Child extends Parent{
	void fun(){ 	//erroe : attempting to assign weaker access previlage; was public
		System.out.println("In child Fun");
	}
}
class Client{
	public static void main(String[] HP){
	
		Parent obj = new Child();
		obj.fun();
	}
}
