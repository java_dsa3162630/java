//private access specifier
class Parent{

	private void fun(){
	
		System.out.println("Parent fun");
	}
}
class Child extends Parent{

	void fun(){
		System.out.println("Child Fun");
	}
}
class Client{

	public static void main(String[] HP){
	
		Parent obj = new Child();
		obj.fun();//error:fun() has private access in parent (private are accessible only in class not outside class) 
	}
}
