class Parent{
	char fun(){		 //return type-char
		System.out.println("Parent fun");
		return 'A';
	}

}
class Child extends Parent{
	int fun(){ 		//return type -int
		System.out.println("Child Fun");
		return 10;
	}
}
class Client{
	public static void main(String[] HP){
	
		Parent obj = new Child();
		obj.fun();
	}
}
//in overriding return type should be same if they are primitive datatypes
//like int,float,char,double,void
