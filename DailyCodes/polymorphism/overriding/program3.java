class Parent{

	Parent(){
		System.out.println("In Parent Constructor");
	}
	void fun(){
		System.out.println("In Parents Fun");
	}
}
class Child extends Parent{				

	Child(){
		System.out.println("In Childs Constructor");
	}
	void fun(int x){
		System.out.println("In Childs Fun");
	}
}
class client{

	public static void main(String[] HP){
		
		Parent obj = new Child();
		obj.fun(); 		// Childs Method Table is as
					// fun()
					// fun(int)
					// as We are not passing any value in obj.fun();
					// it will call to fun() of parents Method
	}
}
