class Parent{

	Parent(){
		System.out.println("In Parents Constructor");
	}
	void fun(int x){
		System.out.println("In Parents Fun");
	}
}
class Child extends Parent{
	
	Child(){
		System.out.println("In Childs Constructor");
	}
	void fun(){
		System.out.println("In Parents Fun");
	}
}
class Client{
	public static void main(String[] HP){
	
		Parent obj = new Child();
		obj.fun(10);
	}
}
