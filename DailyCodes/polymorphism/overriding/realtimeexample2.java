class Match{

	void matchType(){
		System.out.println("T20/ODI/TC");
	}
}
class IPL extends Match{

	void matchType(){
		System.out.println("T20");
	}
}
class TestMatch extends Match{

	void matchType(){
		System.out.println("Test");
	}
}
class Client{

	public static void main(String[] HP){
	
		Match obj = new IPL();
		obj.matchType();//T20

		Match obj1 = new TestMatch();
		obj1.matchType();//Test

		Match obj2 = new Match();
		obj2.matchType();//T20/ODI/TC
	}
}
