class Parent{

	Parent(){
		System.out.println("In Parent Constructor");
	}
	void fun(){
		System.out.println("In Parents Fun");
	}
}
class Child extends Parent{

	Child(){
		System.out.println("In Child Constructor");
	}
	void gun(){
		System.out.println("In Child Fun");
	}
}
class client{

	public static void main(String[] HP){
	
		Child obj = new Child();
		obj.fun();
		obj.gun();

		Parent obj1 = new Parent();
		obj1.fun();
		obj1.gun();	//error :cannot find symbol
				//parent cannot acces childs content
	}
}
