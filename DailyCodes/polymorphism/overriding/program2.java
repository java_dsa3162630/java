class Parent{

	Parent(){
		System.out.println("In Parent Constructor");
	}
	void fun(){
		System.out.println("In Parent fun");
	}
}
class Child extends Parent{

	Child(){
		System.out.println("In Child Constructor");
	}
	void fun(){
		System.out.println("In Childs Fun");
	}
}
class Client{
	public static void main(String[] HP){
	
		Parent obj = new Child();
		//Parents REFRENCE = Childs OBJECT   (At compile time compiler checks REFRENCE's method but at runtime it gives PREFRENCE to OBJECT's method )
		obj.fun();// In Child Fun
	}
}
