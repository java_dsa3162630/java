//static modifier

class Parent{
	static void fun(){
		System.out.println("Parent fun");
	}
}
class Child extends Parent{

	void fun(){	//erroe :overriden method is static(static cannt be override)
		System.out.println("Child fun");
	}
}
class Client{
	public static void main(String[] HP){
		Parent obj = new Child();
		obj.fun();
	}
}
