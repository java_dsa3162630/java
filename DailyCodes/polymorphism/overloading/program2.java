class Demo{

	int Fun(int x){		//Method Signature-fun(int)
		System.out.println(x);
	}

	float Fun(int x){	//Method Signature-fun(int)
		System.out.println(x);
	}
}	// Above two methods have same method signature in method table so ambiguty error
	//tough their return types are different but return type are not considered in method signatures 

