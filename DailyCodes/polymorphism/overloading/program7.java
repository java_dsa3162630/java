class Demo{

	void fun(Object obj){
	
		System.out.println("Object class");
	}
	void fun(String str){
	
		System.out.println("String class");
	}
}
class Client{
	
	public static void main(String[] HP){
	
		Demo obj = new Demo();
		obj.fun("String");//String class
		obj.fun(new StringBuffer("String"));//Object Class
		obj.fun(null);//String class -Priority to child class rather than parent class 

	}
}
