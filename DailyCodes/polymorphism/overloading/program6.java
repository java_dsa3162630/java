class Demo{

	void fun(String str){
		System.out.println("String");
	}
	void fun(StringBuffer Str1){
		System.out.println("StringBuffer");
	}
}
class Client{
	public static void main(String[] HP){
	
		Demo obj = new Demo();
		obj.fun("Pritam");
		obj.fun(new StringBuffer("Pritam"));
		obj.fun(null);//error : refer to fun is ambiguous
	}

}
