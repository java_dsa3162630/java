
import java.util.*;

class Project implements Comparable{

	String pName = null;
	int teamSize = 0;
	int duration = 0;

	Project(String pName, int teamSize, int duration){
	
		this.pName = pName;
		this.teamSize = teamSize;
		this.duration = duration;
	}

	public String toString(){
	
		return "{"+pName+":"+teamSize+":"+duration+"}";
	}

	public int compareTo(Object obj){
	
		//return pName.compareTo(((Project)obj).pName);
		return this.duration - ((Project)obj).duration;
	}
}

class PriorityQueueSort{

	public static void main(String[] args){
	
		PriorityQueue pq = new PriorityQueue();

		pq.offer(new Project("ChatApp",12,45));
		pq.offer(new Project("Browser",9,70));
		pq.offer(new Project("ResumeBuilder",5,90));
		pq.offer(new Project("PatternGenerator",7,30));

		System.out.println(pq);
	}
}
