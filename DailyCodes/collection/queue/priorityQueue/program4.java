
import java.util.*;

class Project{

	String pName = null;
	int teamSize = 0;
	int duration = 0;

	Project(String pName, int teamSize, int duration){
	
		this.pName = pName;
		this.teamSize = teamSize;
		this.duration = duration;
	}

	public String toString(){
	
		return "{"+pName+":"+teamSize+":"+duration+"}";
	}
}

class SortByPName implements Comparator{

	public int compare(Object obj1, Object obj2){
	
		return (((Project)obj1).pName).compareTo(((Project)obj2).pName);
	}
}

class PriorityQueueSort{

	public static void main(String[] args){
	
		PriorityQueue pq = new PriorityQueue(new SortByPName());

		pq.offer(new Project("ChatApp",12,45));
		pq.offer(new Project("Browser",9,70));
		pq.offer(new Project("ResumeBuilder",5,90));
		pq.offer(new Project("PatternGenerator",7,30));

		System.out.println(pq);
	}
}
