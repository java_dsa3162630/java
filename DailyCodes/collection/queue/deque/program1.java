//methods of Deque

import java.util.*;

class DequeDemo{

	public static void main(String[] args){
	
		Deque dq = new ArrayDeque();

		dq.offer(10);
		dq.offer(40);
		dq.offer(30);
		dq.offer(50);
		dq.offer(20);

		System.out.println(dq);

		//offerFirst();
		dq.offerFirst(5);
		//offerLast();
		dq.offerLast(55);
		System.out.println(dq);

		//pollFirst();		
		System.out.println(dq.pollFirst());
		//pollLast();		
		System.out.println(dq.pollLast());
		System.out.println(dq);

		//peekFirst();		
		System.out.println(dq.peekFirst());
		//peekLast();		
		System.out.println(dq.peekLast());
		System.out.println(dq);

		//iterator();
		Iterator itr1 = dq.iterator();
		while(itr1.hasNext()){
			System.out.println(itr1.next());
		}
		
		//descendingIterator();
		Iterator itr2 = dq.descendingIterator();
		while(itr2.hasNext()){
			System.out.println(itr2.next());
		}
	}
}
