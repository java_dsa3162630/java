import java.util.*;

class MyClass{
	
	String name = null;
	MyClass(String name){
		this.name = name;
	}
}
class HashSetDemo{

	public static void main(String[] HP){
		
		HashSet hs = new HashSet();

		hs.add(new MyClass("Pritam"));
		hs.add(new MyClass("Priya"));
		hs.add(new MyClass("Rohit"));
		hs.add(new MyClass("Rohan"));
	
		System.out.println(hs);

	}
}
