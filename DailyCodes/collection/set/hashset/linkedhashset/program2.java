import java.util.*;

class CricPlayer{

	int jerNo = 0;
	String pname = null;
	CricPlayer(int jerNo, String pname){
	
		this.jerNo = jerNo;
		this.pname = pname;
	}
	public String toString(){ // if you dont write this method then o/p will br 4 objects refrences
	
		return jerNo+" : "+pname;
	}
}
class LinkedHashSetDemo{

	public static void main(String[] HP){
	
		LinkedHashSet lhs = new LinkedHashSet();

		lhs.add(new CricPlayer(18,"Virat"));
		lhs.add(new CricPlayer(7,"MSD"));
		lhs.add(new CricPlayer(45,"Rohit"));
		lhs.add(new CricPlayer(7,"MSD"));

		System.out.println(lhs);
	}
}
