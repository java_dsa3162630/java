//methods in SortedSet

import java.util.*;

class SortedSetDemo{

	public static void main(String[] agrs){
	
		SortedSet ss = new TreeSet();

		ss.add("Ganesh");
		ss.add("Rohan");
		ss.add("Dipak");
		ss.add("Suraj");
		ss.add("Jaydeep");

		System.out.println(ss);

		//headSet()
		System.out.println(ss.headSet("Jaydeep"));
		
		//tailSet()
		System.out.println(ss.tailSet("Rohan"));

		//subSet()
		System.out.println(ss.subSet("Ganesh","Jaydeep"));

		//first()
		System.out.println(ss.first());

		//last()
		System.out.println(ss.last());
	}
}
