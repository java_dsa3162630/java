//user Defined TreeSet class

import java.util.*;
class MyClass implements Comparable{

	String Name = null;

	MyClass(String Name){
	
		this.Name = Name;
	}
	public int compareTo(Object obj){
		
		return -(((MyClass)obj).Name).compareTo(this.Name);
	}
	public String toString(){
	
		return Name;
	}
}
class TreeSetDemo{

	public static void main(String[] HP){
	
		TreeSet ts = new TreeSet();

		ts.add(new MyClass("Priya"));
		ts.add(new MyClass("Pritam"));
		ts.add(new MyClass("Rohit"));
		ts.add(new MyClass("Rohan"));
		ts.add(new MyClass("Pritam"));

		System.out.println(ts);
	}
}
