import java.util.*;

class IteratorDemo{

	public static void main(String[] HP){
	
		ArrayList al = new ArrayList();

		al.add("Pritam");
		al.add("Ganesh");
		al.add("Prathmesh");

		System.out.println(al);

		Iterator itr = al.iterator();
		while(itr.hasNext()){
			
			if("Ganesh".equals(itr.next()))
				itr.remove();
			//System.out.println(itr.hasNext()); Exception:NoSuchElemenyFound
		}
		System.out.println(al);
	}

}
