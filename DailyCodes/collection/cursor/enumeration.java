import java.util.*;
class EnumerationDemo{

	public static void main(String[] HP){
	
		Vector v = new Vector();

		v.addElement("Ashish");
		v.addElement("Kanha");
		v.addElement("Rahul");
		v.addElement("Badhe");

		Enumeration cursor = v.elements();

		while(cursor.hasMoreElements()){
		
			System.out.println(cursor.nextElement());
		}
	}
}
