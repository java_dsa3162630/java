// sorting ArrayList using Comparator interface

import java.util.*;

class Employee{

	String name = null;
	float sal = 0.0f;

	Employee(String name, float sal){
	
		this.name = name;
		this.sal = sal;
	}

	public String toString(){
	
		return "{"+name+":"+sal+"}";
	}
}

class SortByName implements Comparator<Employee>{

	public int compare(Employee obj1,Employee obj2){
		
		return obj1.name.compareTo(obj2.name);
	}
}

class SortBySal implements Comparator<Employee>{

	public int compare(Employee obj1,Employee obj2){
		
		return (int)(obj1.sal-obj2.sal);
	}
}

class ListSortDemo{

	public static void main(String[] args){
		
		ArrayList<Employee> al =  new ArrayList<Employee>();

		al.add(new Employee("Ganesh",40.0f));
		al.add(new Employee("Rohan",35.0f));
		al.add(new Employee("Pritam",47.0f));
		al.add(new Employee("Ram",44.0f));

		System.out.println(al);
		
		Collections.sort(al,new SortByName());
		System.out.println(al);
		
		Collections.sort(al,new SortBySal());
		System.out.println(al);
	}
}
