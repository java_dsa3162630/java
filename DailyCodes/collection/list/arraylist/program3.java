//sorting of Arraylist using Collection class

import java.util.*;

class SortDemo{

	public static void main(String[] hp){
	
		ArrayList al = new ArrayList();

		al.add("Kanha");
		al.add("Ashish");
		al.add("Badhe");
		al.add("Rahul");
		al.add("Shashi");

		System.out.println(al);

		Collections.sort(al);
		System.out.println(al);

		//TreeSet ts = new TreeSet(al);  but can contain only unique data
		//System.out.println(ts);

	}
}
