import java.util.*;

class ArrayListDemo extends ArrayList{

	public static void main(String[] HP){
	
		ArrayListDemo al = new ArrayListDemo();
		//add(element)
		al.add(10);
		al.add(20.5f);
		al.add("Pritam");
		al.add(10);
		al.add(20.5f);
		System.out.println(al);

		//add(int,element)
		al.add(3,"Core2Web");
		System.out.println(al);


		//size()
		System.out.println(al.size());
		System.out.println(al);

		//contains(Object)
		System.out.println(al.contains("Core2Web"));
		System.out.println(al.contains(30));

		//indexOf(Object)
		System.out.println(al.indexOf(20.5f));
		System.out.println(al.indexOf("Core2Web"));

		//lastIndexOf(object)
		System.out.println(al.lastIndexOf(10));

		//get(int)
		System.out.println(al.get(3));

		//set(index,element)
		System.out.println(al.set(3,"Incubator"));
		System.out.println(al);

		
		ArrayList al2 = new ArrayList();
		al2.add("salman");
		al2.add("shahrukh");
		al2.add("aamir");

		System.out.println(al2);

		//addAll(collection)
		al.addAll(al2);
		System.out.println(al);

		//addAll(index,collection)
		al.addAll(3,al2);
		System.out.println(al);

		//removeRange(int,int)
		al.removeRange(3,6);
		System.out.println(al);

		//remove(int)
		System.out.println(al.remove(3));
		System.out.println(al);

		//toArray() (object[] toArray())
		Object arr[] = al.toArray();
		for(Object data : arr){
			System.out.print(data +" ");
		}
		System.out.println();

		//clear()
		al.clear();
		System.out.println(al);
	}
}
