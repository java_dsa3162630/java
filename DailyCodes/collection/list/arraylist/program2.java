//Storing user defined objects in ArrayList
import java.util.*;

class CricPlayer{
	
	int JerNo = 0;
	String Name = null;

	CricPlayer(int JerNo, String Name){
	
		this.JerNo = JerNo;
		this.Name = Name;
	}

	public String toString(){
	
		return JerNo+":"+Name;
	}	
}
class ArrayListDemo{

	public static void main(String[] HP){
	
		ArrayList al = new ArrayList();
		al.add(new CricPlayer(18,"virat"));
		al.add(new CricPlayer(7,"Dhoni"));
		al.add(new CricPlayer(45,"Rohit"));

		System.out.println(al);

		al.add(1,new CricPlayer(33,"Hardik"));
		System.out.println(al);
	}
}
