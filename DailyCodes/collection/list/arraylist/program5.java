// sorting ArrayList using Comparator interface

import java.util.*;

class Movies{

	String name = null;
	float colltn = 0.0f;
	float imdb = 0.0f;

	Movies(String name, float colltn, float imdb){
	
		this.name = name;
		this.colltn = colltn;
		this.imdb = imdb;
	}

	public String toString(){
	
		return "{"+name+"|"+colltn+"|"+imdb+"}";
	}
}

class SortByName implements Comparator{

	public int compare(Object obj1,Object obj2){
		
		return ((Movies)obj1).name.compareTo(((Movies)obj2).name);
	}
}

class SortByColltn implements Comparator{

	public int compare(Object obj1,Object obj2){
		
		return (int)(((Movies)obj1).colltn-((Movies)obj2).colltn);
	}
}

class SortByImdb implements Comparator{

	public int compare(Object obj1,Object obj2){
		
		return (int)(((Movies)obj1).imdb-((Movies)obj2).imdb);
	}
}

class ListSortDemo{

	public static void main(String[] args){
		
		ArrayList al =  new ArrayList();

		al.add(new Movies("Interstellar",700.0f,9.9f));
		al.add(new Movies("Ford vs Ferrari",650.0f,8.5f));
		al.add(new Movies("Social Networking",470.0f,9.0f));
		al.add(new Movies("Night Crawller",500.0f,8.8f));
		
		System.out.println(al);
		
		Collections.sort(al,new SortByName());
		System.out.println(al);
		
		Collections.sort(al,new SortByColltn());
		System.out.println(al);
		
		Collections.sort(al,new SortByImdb());
		System.out.println(al);
	}
}
