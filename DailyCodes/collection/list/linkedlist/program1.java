import java.util.*;

class LinkedListDemo{

	public static void main(String[] HP){
	
		LinkedList ll = new LinkedList();

		ll.add(20);
		ll.addFirst(10);
		ll.addLast(30);
		System.out.println(ll);

		ll.add(2,25);
		System.out.println(ll);

		System.out.println(ll.getFirst());
		
		System.out.println(ll.getLast());

		ll.remove();
		System.out.println(ll);
		
		ll.removeLast();
		System.out.println(ll);

	}
}


