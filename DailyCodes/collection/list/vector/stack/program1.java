import java.util.*;

class StackDemo{

	public static void main(String[] HP){
	
		Stack s = new Stack();

		s.add(10);
		s.add(20);
		s.add(30);
		s.add(40);

		System.out.println(s);

		s.pop();
		System.out.println(s);

		System.out.println(s.peek());
		System.out.println(s);

		System.out.println(s.empty());

		
	}
}
