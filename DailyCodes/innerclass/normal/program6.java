class Outer{

	int a = 10;
	static int b = 20;

	class Inner{
	
		int x = 30;
		static int y = 40;	//error: illegal static declaration in inner class Outer.Inner
		//final static int b = 40;
	}
}
