class Outer{

	class Inner{
	
		void M1(){
			System.out.println("INNER M1");
		}
	}
	void M2(){
	
		System.out.println("OUTER M2");
	}
}
class Client{

	public static void main(String[] HP){
	
		Outer obj = new Outer();
		obj.M2();

		//Outer.Inner obj1 = obj.new Inner();
		
		Outer.Inner obj1 = new Outer().new Inner();
		obj1.M1();
	}
}
