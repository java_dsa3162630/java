class Outer{

	void m1(){
	
		System.out.println("In M1 Outer");
	}
	static class Inner{
	
		void m1(){
		
			System.out.println("In M1 Inner");
		}
	}
}
class Client{

	public static void main(String[] HP){
	
		Outer obj = new Outer();
		obj.m1();
		
		Outer.Inner obj1 = new Outer.Inner();
		obj1.m1();
	}
}
